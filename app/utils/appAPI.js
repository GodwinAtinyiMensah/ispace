import $ from 'jquery';
import toastr from 'toastr';
import {setEvents, setUser, setGallery, setCollection, setBlogPosts, setVideos} from "../actions/AppActions";

var sample = [
    {
        "Gallery": {
            "Name": "iSpace Accelerator Program",
            "Created": "0000-12-31T23:59:08+00:00",
            "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
        },
        "Images": [
            {
                "Name": "_DSC0530(2)-2.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0522(3).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0515.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0510(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0531(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0510.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0518(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0533(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0509.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0520(3).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            }
        ]
    },
    {
        "Gallery": {
            "Name": "Phoenix Kids",
            "Created": "0000-12-31T23:59:08+00:00",
            "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
        },
        "Images": [
            {
                "Name": "DSC_9340.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-103.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-119.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-114.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "DSC_9350.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "DSC_9335.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "DSC_9353.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-142.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            }
        ]
    },
    {
        "Gallery": {
            "Name": "Unlocking Women And Technology",
            "Created": "0000-12-31T23:59:08+00:00",
            "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
        },
        "Images": [
            {
                "Name": "ddsseee.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0049.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_1310.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0048.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0035.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0046.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            }
        ]
    }
]

var y = [{"Name":"iSpace Accelerator Program","Created":"0001-01-01T00:00:00Z","Id":"0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"},{"Name":"Phoenix Kids","Created":"0001-01-01T00:00:00Z","Id":"8febd14e-c2b9-40d8-a425-0f2069c3678c"},{"Name":"","Created":"0001-01-01T00:00:00Z","Id":"749491f8-0602-4ada-9c70-780705b7bbda"},{"Name":"Unlocking Women And Technology","Created":"0001-01-01T00:00:00Z","Id":"46fe4fb0-2958-46bb-ba26-a8c69780fa64"}]

export function signInApi(user) {
	$.ajax({
	    url: '/api/login',
	    method: "POST",
	    data: user,
	    cache: false,
	    success: function(data) {
	      var user = JSON.parse(data)
	      setUser(user);
	      if (user.LoggedIn == true) { 
		    location.hash = "#/dashboard/addevents"
		  }
	    }.bind(this),
	    error: function(xhr, status, err){
	      console.log(err);
	    }.bind(this)
	})
}

export function signUpApi(user) {
	$.ajax({
	    url: '/api/signup',
	    method: "POST",
	    data: user,
	    cache: false,
	    error: function(xhr, status, err){
	      console.log(err);
	    }.bind(this)
	})
}

export function addEventApi(data) {
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "/api/addEvents", true);
	xhr.onload = function() {
		if (xhr.status === 200) {
			console.log("Success")
		}else {
			console.log("Errors")
		}
	}
	xhr.send(data)
}

export function addBlogPostApi(data) {
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "/api/newBlogPost", true);
	xhr.onload = function() {
		if (xhr.status === 200) {
			console.log("SENT MEEE")
		}else {
			console.log("NOT SENT")
		}
	}
	xhr.send(data)
}

export function addVideoApi(data) {
	$.ajax({
	    url: '/api/addVideo',
	    method: "POST",
	    data: data,
	    cache: false,
	    error: function(xhr, status, err){
	      console.log(err);
	    }.bind(this)
	})
}

export function addGalleryApi(data) {
	$.ajax({
	    url: '/api/addGallery',
	    method: "POST",
	    data: data,
	    cache: false,
	    error: function(xhr, status, err){
	      console.log(err);
	    }.bind(this)
	})
}


export function getGalleryApi() {
	setGallery(y);
}


export function addImageApi(data) {
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "/api/addImage", true);
	xhr.onload = function() {
		if (xhr.status === 200) {
			console.log("SENT MEEE")
		}else {
			console.log("NOT SENT")
		}
	}
	xhr.send(data)
}

export function tourRequestApi(json) {
	$.ajax({
		url: '/api/tourRequest',
		method: "POST",
		data: json,
		cache: false,
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function partnerRequestApi(json) { 
	$.ajax({
		url: '/api/partnerRequest',
		method: "POST",
		data: json,
		cache: false,
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function programRequestApi(json) {
	$.ajax({
		url: '/api/programRequest',
		method: "POST",
		data: json,
		cache: false,
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function memberRequestApi(json) {
	$.ajax({
		url: '/api/memberRequest',
		method: "POST",
		data: json,
		cache: false,
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function spaceBookingApi(json) {
	$.ajax({
		url: '/api/spaceBooking',
		method: "POST",
		data: json,
		cache: false,
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function subscriptionApi(json) {
	$.ajax({
		url: '/api/subscription',
		method: "POST",
		data: json,
		cache: false,
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function getEventsApi() {
	$.ajax({
		url: '/api/getEvents',
		method: "POST",
		cache: false,
		success: function(data) {
	      setEvents(data);
	    },
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function getCollectionApi() {
	setCollection(sample);
}

export function getBlogPostsApi() {
	$.ajax({
		url: '/api/getBlogPost',
		method: "POST",
		cache: false,
		success: function(data) {
	      setBlogPosts(data);
	    },
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function getVideosApi() {
	$.ajax({
		url: '/api/getVideo',
		method: "POST",
		cache: false,
		success: function(data) {
	      setVideos(data);
	    },
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
}

export function applyApi(data) {
	$.ajax({
		url: '/api/apply',
		method: "POST",
		data: data,
		cache: false,
		error: function(xhr, status, err){
			console.log(err);
		}.bind(this)
	})
	toastr.success("Thanks For Applying");
}