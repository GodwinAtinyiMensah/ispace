import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class BlogPost extends React.Component {

  goback() {
	window.history.back();
  }

  render() {
  	const font = {
	  	fontSize: '50px',
	}
    return (
		<div className="blogPost">
			<div className="container">
			  <div className="row">
			    <div className="twelve column">
			    	<div className="head">
			    		<i className="material-icons goback" onClick={this.goback.bind()} style={{font}}>navigate_before</i>
			    		<img src="assets/images/bg1.jpg" />
			    	</div>
			    </div>
			  </div>
			</div>
			<div className="container">
			  <div className="row">
			    <div className="twelve column">
			      	<div className="body">
			      		<h1>{this.props.data.Title}</h1>
			      		<p>
			      			{this.props.data.Body}
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      			ksdmfksmdflksmdlkfslkd'dpmfkdmfamkasmddlasdkmaskmdaksmdaaslkmaskmdalsmdlaksmlakmsdlkamsdaksda
			      		</p>
			      	</div>
			    </div>
			  </div>
			 </div>	 
		</div>
    );
  }

}









