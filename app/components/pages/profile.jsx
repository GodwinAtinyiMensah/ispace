import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Profile extends React.Component {
  
  goback() {
	window.history.back();
  }

  render() {
  	const font = {
	  	fontSize: '50px',
	}
    return (
		<div className="profile">
			<div className="container">
			  <div className="row">
			    <div className="twelve column">
			    	<i className="material-icons goback" onClick={this.goback.bind()} style={{font}}>navigate_before</i>
			    	<h1>{this.props.data.Name}</h1>
			      	<div className="body">
			      		<div className="pic">
			      			<img src={this.props.data.Picture} />
			      		</div>
			      		<div className="about">
			      			<h6>{this.props.data.Position}</h6>
			      			<a href={this.props.data.Social.facebook} target="blank"><i className="fa fa-facebook"></i></a>
						    <a href={this.props.data.Social.twitter} target="blank"><i className="fa fa-twitter"></i></a>
						    <a href={this.props.data.Social.linkedIn} target="blank"><i className="fa fa-linkedin"></i></a>
			      			<p>
			      				{this.props.data.About}
			      			</p>
			      		</div>
			      	</div>
			    </div>
			  </div>
			 </div>	 
		</div>
    );
  }

}









