import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Galleryview extends React.Component {
  
  constructor(props) {
  	super(props)
  	this.images = this.images.bind(this);
  }

  images(list) {
  	if (list) {
  		return list.Images.map(function(item, i) {
	  		return (
  				<img key={i} src={"https://storage.googleapis.com/igallary/" + item.Name} /> 
  				
	  		)
	  	})
  	}
  	
  }

  goback() {
	window.history.back();
  }

  render() {
  	const font = {
	  	fontSize: '50px',
	}
    return (
		<div className="galleryview">
			<div className="container">
			  <div className="row">
			    <div className="twelve column">
			    	<i className="material-icons goback" onClick={this.goback.bind()} style={{font}}>navigate_before</i>
			    	<h1>PHOTOS</h1>
			      	<div className="pictures">
			      		{this.images(this.props.list)}
			      	</div>
			    </div>
			  </div>
			 </div>	 
		</div>
    );
  }

}









