import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
//import Box from './eventsHosting';
import {spaceBooking} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';



export default class Eventsupcoming extends React.Component {

  constructor(props) {
  	super(props)
    this.handleChange = this.handleChange.bind(this);
    this.getMonth = this.getMonth.bind(this);
    this.events = this.events.bind(this);
    this.state = {
    	showBox: "false",
    	currentMonth: "January",
    	details: {},
    }
  }

  
  handleChange() {
  	var month = document.querySelector('select').value;
  	this.setState({
  		currentMonth: month,
  		showBox: "false",
  	})
  }

  events(month) {
  	var list = this.props.list;
  	var filtered = list.filter(function(item) {
  		return item.Month === month;
  	})
  	if (filtered.length === 0) {
  		return <Noevents />
  	}else {
  		return filtered.map(function(item, i) {
		  	return (

				<div key={i} onClick={() => {this.showDetails(item)}}>
					<div id="item">
					   	<h5>{item.Day}</h5>
					   	<span>th</span>
					</div>
				</div> 
		  		);
  		}.bind(this));
  	}
  	
  }

  showBox(option) {
  	this.setState({
  		showBox: option,
  	})
  }

  showDetails(option) {
  	this.setState({
  		details: option,
  		showBox: "details",
  	})
  }

  getMonth() {
  	$.ajax({
	    url: 'http://api.timezonedb.com/v2/get-time-zone?key=A2JAVXJD0XMQ&format=json&by=zone&zone=Africa/Accra',
	    method: "GET",
	    success: function(data) {
	      if (data) {
	      	var formatted = data.formatted;
	      	var month = formatted.slice(5, 7);
	      	var data = eventMonths("04");
			$("select").append(data);
			this.handleChange();
	      }
	    }.bind(this),
	    error: function(xhr, status, err){
	      console.log(err);
	    }.bind(this)
	})
  }


  componentDidMount() {
  	this.getMonth();
  }

  render() {
  	var month;
  	var box;
  	var details;
  	if (this.state.showBox == "false") {
  		box = ""
  	}else if (this.state.showBox == "details") {
  		details = <Details data={this.state.details} />
  	}else {
  		box = <Box box={this.showBox.bind(this, "false")} />
  	}

    return (
		<div className="eventpage">
			{box}
			<div className="container">
			  <div className="row">
			    <div className="twelve column subheaders event-header1">
			      <div className="home">
			        <div className="logo">
			          <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
			        </div>
			        <div className="caption">
			          <h1>UPCOMING EVENTS</h1>
			          <div>
			            <a onClick={this.showBox.bind(this, "true")}>Book Our Space</a>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<div className="month">
			   	<div className="row">
			     	<div className="twelve column">
			     		<h4>OUR CURRENT EVENTS</h4>
			     		<p>Click the button to Navigate.</p>
			     		<select onChange={this.handleChange}>
			     		</select>
			     	</div>
			    </div>
			</div>
			<div className="container upcoming">
			   <div className="row">
			     <div className="twelve column">
			      <div className="subpage">
			        <div className="list">
			        	{
			        		this.events(this.state.currentMonth)
			        	}
			        </div>
			      </div>
			     </div>
			   </div>
			 </div>
			{details}
		</div>
    );
  }

}

const Noevents = () => (
	<section className="Noevents">
		<h1>No Events Yet ......</h1>
	</section>
	)


// <HyperLink name="EVENT HOSTING" link="#/event-hosting" />

export class Box extends React.Component {
	spaceBooking(e) {
	  	e.preventDefault();
	  	var name = e.target.name.value;
	    var email = e.target.email.value;
	   	var number = e.target.number.value;

	   	if (name !== "" && email !== "" && number !== "") {
		   	var data = {
		   		name: name,
		   		email: email,
		    	number: number,
		    }
		    spaceBooking(data)
		}

		e.target.name.value = "";
	    e.target.email.value = "";
	    e.target.number.value = "";

	    $("form").hide();
	    $("#submitbox").append(`<div class='thanks'><h2>Thank You ${name}</h2><p>We will get back to you soon</p></div>`);
	}

	render() {
	    return (
	    		<div id="submitbox">
					<form onSubmit={this.spaceBooking.bind(this)}>
						<h4>BOOK OUR SPACE</h4>
				    	<input type="text" name="name" placeholder="Your Name" />
				    	<input type="email" name="email" placeholder="Your Email" />
					   	<input type="text" name="number" placeholder="Your Number" />
						<button>SUBMIT</button>
			    	</form>
			    	<a onClick={this.props.box}>Cancel</a>
			    </div>
			)
	}

}

export class Details extends React.Component {
	render() {
		if (this.props.data) {
			return (
	    		<div className="details">
	    			<div className="about">
					    <h4>{this.props.data.Name}</h4>
					    <p>
					   		{this.props.data.Summary}
				    	</p>
				    </div>
			    	<div className="organizers">
			    		<img src={"https://storage.googleapis.com/ievents/" + this.props.data.Image} />
			    		<a href={this.props.data.Link} target="_blank">REGISTER</a>
			    	</div>
			    </div>
			)
		}
	}

}


function eventMonths(data) {
	switch(data) {
		case "02":
			return (
				`
					<option>February</option>
					<option>March</option>
				    <option>April</option>
		 			<option>May</option>
				    <option>June</option>
		 			<option>July</option>
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				`
			);
		break

		case "03":
			return (
				`
					<option>March</option>
				    <option>April</option>
		 			<option>May</option>
				    <option>June</option>
		 			<option>July</option>
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				`
			);
		break

		case "04":
			return (
				`
				    <option>April</option>
		 			<option>May</option>
				    <option>June</option>
		 			<option>July</option>
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				`
			);
		break

		case "05":
			return (
				`				    
		 			<option>May</option>
				    <option>June</option>
		 			<option>July</option>
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				`
			);
		break

		case "06":
			return (
				`
				    <option>June</option>
		 			<option>July</option>
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				    <option>May</option>
				`
			);
		break

		case "07":
			return (
				`
		 			<option>July</option>
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				    <option>May</option>
				    <option>June</option>
				`
			);
		break

		case "08":
			return (
				`					
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				    <option>May</option>
				    <option>June</option>
				    <option>July</option>
				`
			);
		break

		case "09":
			return (
				`
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				    <option>May</option>
				    <option>June</option>
				    <option>July</option>
				    <option>August</option>
				`
			);
		break

		case "10":
			return (
				`
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				    <option>May</option>
				    <option>June</option>
				    <option>July</option>
				    <option>August</option>
				    <option>September</option>
				`
			);
		break

		case "11":
			return (
				`
				    <option>November</option>
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				    <option>May</option>
				    <option>June</option>
				    <option>July</option>
				    <option>August</option>
				    <option>September</option>
				    <option>October</option>
				`
			);
		break

		case "12":
			return (
				`
				    <option>December</option>
				    <option>January</option>
				    <option>February</option>
				    <option>March</option>
				    <option>April</option>
				    <option>May</option>
				    <option>June</option>
				    <option>July</option>
				    <option>August</option>
				    <option>September</option>
				    <option>October</option>
				    <option>November</option>
				`
			);
		break

	default:
		return (
				`
					<option>January</option>
					<option>February</option>
					<option>March</option>
				    <option>April</option>
		 			<option>May</option>
				    <option>June</option>
		 			<option>July</option>
				   	<option>August</option>
		 			<option>September</option>
				    <option>October</option>
				    <option>November</option>
				    <option>December</option>
				`
			);
	}
}


