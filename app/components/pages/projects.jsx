import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import Tourbutton from '../layouts/tourButton';
import { tourRequest, programRequest} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Projects extends React.Component {

  constructor(props) {
  	super(props)
    this.state = {
    	showBox: "false",
    }
  }

  showBox(option) {
  	this.setState({
  		showBox: option,
  	})
  }

  render() {
  	var box;
  	if (this.state.showBox == "false") {
  		box = ""
  	}else {
  		box = <Box box={this.showBox.bind(this, "false")} />
  	}
    return (
		<div>
			{box}
			<div className="container">
			  	<div className="row">
				    <div className="twelve column subheaders prog-header1">
				      	<div className="home">
					        <div className="logo">
					         <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
					        </div>
				        <div className="caption">
					        <h1>OUR PROJECTS</h1>
					        <div>
					            <HyperLink name="BECOME A PARTNER" link="#/partners" />
					            <a onClick={this.showBox.bind(this, "true")}>Join Our Programs</a>
					        </div>
				        </div>
				      </div>
				    </div>
				</div>
			 </div>
			<div className="projects">
			    <div className="item">
			     	<img className="logo" src="https://storage.googleapis.com/iimages/waziup_logo.png" />
			     	<div className="about">
			     		<h3>Waziup</h3>
			     		<p>
			     			The WAZIUP project, namely the Open Innovation Platform for IoT-Big Data in Sub-Saharan Africa is a collaborative research project using cutting edge technology applying IoT and Big Data to improve the working conditions in the rural ecosystem of Sub-Saharan Africa. 

							The consortium of WAZIUP involves 7 partners from 4 African countries and partners from 5 EU countries combining business developers, technology experts and local Africa companies operating in agriculture and ICT. The project involves also regional hubs, namely the iSpace Foundation, with the aim to promote the results to the widest base in the region. 
			     		</p>
			     	</div>	
			     	<div className="team">
			     		<img className="fix" src="https://storage.googleapis.com/iimages/team/hali.jpeg" />
			     		<h5>Halima Haruna</h5>
			     		<h6>Project Manager</h6>
			     	</div>
			    </div>
			    <div className="item">
			    	<img className="logo" src="https://storage.googleapis.com/iimages/improve.png" />
			     	<div className="about">
			     		<h3>Save Ghana</h3>
			     		<p>
				     		IMPROVE is an attendance biometric system for fingerprint detection which enables teachers to be able to login to the system with their unique fingerprints. The fingerprint of the teacher is compared with that which has been stored in the database and if it matches the data it then goes further to authenticate.

							The system also generates a brief report of attendance from the database according to time-wise, date-wise and also provides GPS location of every clocking in of a teacher.

							Fingerprint identification is also fraud proof as well as user friendly. The need for swipe cards or ID cards is eliminated as the software will require the person being authenticated to be present at the time and point of authentication. 
			     		</p>
			     	</div>	
			     	<div className="team">
			     		<img src="https://storage.googleapis.com/iimages/team/hali.jpeg" />
			     		<h5>Halima Haruna</h5>
			     		<h6>Project Manager</h6>
			     	</div>
			    </div>
			    <div className="item">
			     	<img className="logo" src="https://storage.googleapis.com/iimages/edenlablogo.png" />
			     	<div className="about">
			     		<h3>Eden Labs</h3>
			     		<p>
			     			Eden Labs is a creative space where young and talented individuals are taught through workshops which are purely practical. Eden Labs encourages people to explore, innovate  and  create their ideas in new technologies such as 3D, IOT (Internet of things) etc. We also have 3D printers and hardware devices such as raspberry pi, arduino etc useful in hardware projects.
			     		</p>
			     	</div>	
			     	<div className="team">
			     		<img src="https://storage.googleapis.com/iimages/team/godwin.jpg" />
			     		<h5>Godwin Mensah</h5>
			     		<h6>Project Manager</h6>
			     	</div>
			    </div>
			    
			  </div>
		</div>
    );
  }

}



class Box extends React.Component {

	programRequest(e) {
	  	e.preventDefault();
	  	var name = e.target.name.value;
	    var email = e.target.email.value;
	   	var number = e.target.number.value;
	   	var program = e.target.program.value;

	   	if (name !== "" && email !== "" && number !== ""&& program !== "") {
		   	var data = {
		   		name: name,
		   		email: email,
		    	number: number,
		    	program: program,
		    }
		}

	    e.target.name.value = "";
	    e.target.email.value = "";
	    e.target.number.value = "";
	    e.target.program.value = "";
	    programRequest(data)
	    $("form").hide();
	    $("#submitbox").append(`<div class='thanks'><h2>Thank You ${name}</h2><p>We will get back to you soon</p></div>`);
	}

	render() {
	    return (
	    		<div id="submitbox">
			      	<h4>JOIN OUR PROGRAMS</h4>
			       	<form onSubmit={this.programRequest.bind(this)}>
			       		<input type="text" placeholder="Your Name" name="name"/>
			       		<input type="text" placeholder="Your Email" name="email" />
			       		<input type="text" placeholder="Your Number" name="number" />
			       		<select name="program">
			       			<option>CODE TO STARTUP</option>
			       			<option>UNLOCKED AT iSPACE</option>
			       			<option>UNLOCKING WOMEN and TECHNOLOGY</option>
			       			<option>iSPACE ACCELERATOR PROGRAM</option>
			       		</select>
			       		<button>SUBMIT</button>
			       	</form>
			       	<a onClick={this.props.box}>Cancel</a>
			    </div>
			)
	}

}





