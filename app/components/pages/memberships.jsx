import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import Tourbutton from '../layouts/tourButton';
import {memberRequest} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Memberships extends React.Component {

  constructor(props) {
  	super(props)
    this.state = {
    	showBox: "false",
    }
  }

  showBox(option) {
  	this.setState({
  		showBox: option,
  	})
  }


  render() {
  	var box;
  	if (this.state.showBox == "false") {
  		box = ""
  	}else {
  		box = <Box box={this.showBox.bind(this, "false")} />
  	}
  	
    return (
		<div className="memberships">
			{box}
			<div className="container">
			  <div className="row">
			    <div className="twelve column subheaders memb-header1">
			      <div className="home">
			        <div className="logo">
			         <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
			        </div>
			        <div className="caption">
			          <h1>MEMBERSHIPS</h1>
			          <h5>JOIN OUR FAMILY.</h5>
			          <a onClick={this.showBox.bind(this, "true")}>Become A Member</a>
			          <HyperLink name="PARTNER WITH US" link="#/partners" />
			        </div>
			      </div>
			    </div>
			  </div>
			 </div>
			 <div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="mem-descp">
			        
			        <h2>Our Memberships</h2>
			      </div>
			     </div>
			   </div>
			 </div>

			 <div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="subpage">
			       <div className="list">
						<div id="black">
							<h3>BLACK MEMBER</h3>
				      		<p>As a full member you will be entitled to our complete range of services. iSpace will only admit startups that share parallel interests and objectives.</p>
						</div> 
						<div id="white">
						    <h3>WHITE MEMBER</h3>
				      		<p>Membership at this level is open to our community and we welcome anyone with an idea and the crazy passion to build on it.</p>
						</div>
						<div id="red">
						    <h3>RED MEMBER</h3>
				      		<p>Whilst not only for technology based startups. Membership is available to an individual or a startup with fewer than 3 staff.  Must be working on an idea or interest in developing an MVP. </p>
						</div>						
					</div>
			      </div>
			     </div>
			   </div>
			</div>
			 <div className="container" id="join">
					  <div className="row">
					    <div className="twelve column coworking">
					      <div>
					        <h5>Co-working Space</h5>
					        <p>If you’re a freelancer/entrepreneur  in Accra and looking for an affordable space to work then our co-working space  could just be the answer.</p>
					      </div>
					      <div>
					        <h5>Private Offices</h5>
					        <p>Ideal for 2- 5 person team, Our Private Office is your unique space tailored to your needs. We provide the necessary furniture but you are welcome to bring your own. </p>
					      </div>
					    </div>
					  </div>
					 </div>
			<div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="mem-descp">
			        
			        <p className="sub">We would like to meet first with any company or individual that may be considering joining us. This will allow us to take you through our member offering and explain our joining process.</p>
			      </div>
			     </div>
			   </div>
			</div>
			
			<div className="features">
			   <h5>YOUR MEMBERSHIP INCLUDES....</h5>
			   <div className="icons">
			     <div>
			       <i className="material-icons">location_on</i>
			       <h6>CONVENIENT LOCATION</h6>
			     </div>
			     <div>
			       <i className="material-icons">group_work</i>
			       <h6>AWESOME WORKSPACE</h6>
			     </div>
			     <div>
			       <i className="material-icons">wifi</i>
			       <h6>RELIABLE INTERNET</h6>
			     </div>
			     <div>
			       <i className="material-icons">person_pin</i>
			       <h6>PRIVATE OFFICES</h6>
			     </div>
			     <div>
			       <i className="material-icons">people_outline</i>
			       <h6>COMMUNITY</h6>
			     </div>
			     <div>
			       <i className="material-icons">work</i>
			       <h6>MEETING ROOM</h6>
			     </div>
			   </div>
			 </div>
		</div>
    );
  }

}


class Box extends React.Component {
	memberRequest(e) {
	  	e.preventDefault();
	  	var name = e.target.name.value;
	    var email = e.target.email.value;
	   	var number = e.target.number.value;
	   	var membership = e.target.membership.value;
	    e.target.name.value = "";
	    e.target.email.value = "";
	    e.target.number.value = "";
	    e.target.membership.value = "";
	   	var data = {
	   		name: name,
	   		email: email,
	    	number: number,
	    	membership: membership,
	    }
	    memberRequest(data)
	    $("form").hide();
	    $("#submitbox").append(`<div class='thanks'><h2>Thank You ${name}</h2><p>We will get back to you within 24 hours.</p></div>`);
	}

	render() {
	    return (
	    		<div id="submitbox">
			       	<form onSubmit={this.memberRequest.bind(this)}>
			       		<h4>JOIN OUR FAMILY</h4>
			       		<input type="text" placeholder="Your Name" name="name"/>
			       		<input type="text" placeholder="Your Email" name="email" />
			       		<input type="text" placeholder="Your Number" name="number" />
			       		<select name="membership">
			       			<option>Black Member</option>
			       			<option>White Member</option>
			       			<option>Red Member</option>
			       		</select>
			       		<button>SUBMIT</button>
			       	</form>
			       	<a onClick={this.props.box}>Cancel</a>
			    </div>
			)
	}

}




