import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import Tourbutton from '../layouts/tourButton';
import { programRequest, apply } from '../../actions/AppActions';

export default class Programs extends React.Component {

  constructor(props) {
  	super(props)
  	
  	this.register = this.register.bind(this);

    this.state = {
    	showBox: "false",
    }
  }

  register(e) {
  	e.preventDefault();
  	var fname = e.target.fname.value;
    var lname = e.target.lname.value;
    var email = e.target.email.value;
    var number = e.target.number.value;
    var course = e.target.course.value;
    var gender = e.target.gender.value;
    var city = e.target.city.value;
    var region = e.target.region.value;
    var reason = e.target.reason.value;

    e.target.fname.value = "";
    e.target.lname.value = "";
    e.target.email.value = "";
    e.target.number.value = "";
    e.target.city.value = "";
    e.target.reason.value = "";

   	var data = {
   		fname: fname,
   		lname: lname,
   		email: email,
   		number: number,
   		course: course,
   		gender: gender,
   		city: city,
   		region: region,
   		reason: reason
    }

    apply(data)
  }

  showBox(option) {
  	this.setState({
  		showBox: option,
  	})
  }

  render() {
  	var box;
  	if (this.state.showBox == "false") {
  		box = ""
  	}else {
  		box = <Box box={this.showBox.bind(this, "false")} />
  	}
    return (
		<div className="programs">
			{box}
			<div className="container">
			  <div className="row">
			    <div className="twelve column subheaders prog-header1">
			      <div className="home">
			        <div className="logo">
			         <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
			        </div>
			        <div className="caption">
			          <h1>OUR PROGRAMS</h1>
			          <div>
			            <HyperLink name="OUR PROJECTS" link="#/projects" />
			            
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			 </div>
			 <div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="subpage">
			        <h4>APPLY FOR CODE SCHOOL.</h4>
			      </div>
			     </div>
			   </div>
			 </div>
			 <div className="code-school-register">
			    <p>
			    	Code powers our digital world. Every website, smartphone app, computer programme relies on code in order to operate. This makes coders the designers and builders of the digital age. Code School is a three part program comprising Beginner, Intermediate and Advanced classes that aims to train people on how to build Mobile and Web Apps. This a fun but serious program which is in its 2nd year. 
			    </p>
			    <h6>OBJECTIVE OF THE SCHOOL</h6>
			    <p>
			    	1. To make you employable in the Tech Industry <br />
					2. To build your ideas which will lead into building a successful business.
			    </p>
			    <h6>TUITION FEES</h6>
			    <p>
			    	Beginners: GHC 325.00 (50% to be paid before you join)
			    </p>
			    <h6>Here's how applying to iSpace Code School works: </h6>
			    <p>
			    	1. Fill out the form below <br />
					2. After reviewing your application, we will invite you for an interview.
			    </p>
			    <p>
			    	You'll be amazed at how much you can learn in an immersive environment. We're confident that taking this course will be one of best decisions you'll ever make. 
					The only thing we ask for is your firm commitment to working dedicatedly.
			    </p>
			    <h6>APPLY FOR THE BEGINNER CLASS</h6>
			    <form onSubmit={this.register}> 
			    	<label>
			    		First Name
			    		<input type="text" name="fname" />
			    	</label>
			    	<label>
			    		Last Name
			    		<input type="text" name="lname" />
			    	</label>
			    	<label>
			    		Email
			    		<input type="email" name="email" />
			    	</label>
			    	<label>
			    		Number
			    		<input type="number" name="number" />
			    	</label>
			    	<label>
			    		Course
			    		<select name="course" >
			    			<option>Python</option>
			    			<option>Java</option>
			    		</select>
			    	</label>
			    	<label>
			    		Gender
			    		<select name="gender" >
			    			<option>Male</option>
			    			<option>Female</option>
			    		</select>
			    	</label>
			    	<label>
			    		City
			    		<input type="text" name="city" />
			    	</label>
			    	<label>
			    		Region
			    		<select name="region">
			              <option>Greater Accra Region</option>
			              <option>Ashanti Region</option>
			              <option>Central Region</option>
			              <option>Brong-Ahafo Region</option>
			              <option>Eastern Region</option>
			              <option>Northern Region</option>
			              <option>Upper East Region</option>
			              <option>Upper West Region</option>
			              <option>Volta Region</option>
			            </select>
			    	</label>
			    	<label className="essay">
			    		Why do you want to learn how to code? (400 characters)
			    		<textarea name="reason" maxLength="400"></textarea>
			    	</label>
			    	<button>Apply</button>
			    </form>
			 </div>
			     
			 <div className="container">
			  <div className="row">
			    <div className="twelve column bgimg3">
			      <div className="home">
			        <h4>UNLOCKING WOMEN and TECHNOLOGY</h4>
			      </div>
			    </div>
			  </div>
			 </div>
			 <div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="mem-descp">
			        <p>
			        	Unlocking Women and Technology's aim is to harness the use of technology in mobilising women and empower them to innovate through technology.Recognising that women are significantly under-represented in both the Technology and Startup ecosystem even with various organisations and business call to action to bring more women into the fold. iSpace and partners through Unlocking Women andTechnology program are taking a proactive approach to bring solutions.
			        	The program focuses on helping to remove barriers to science for girls and women. It then builds on their entrepreneurial passion to kickstart a startup. There are 3 Main Stages to the UWAT Program and all these stages focus on different development skills needed to build a Startup.
			        </p>
			        <a target="_blank" href="http://unlockingwat.com/">Read More On Our Website</a>
			      </div>
			     </div>
			   </div>
			 </div>

			 <div className="container">
			  <div className="row">
			    <div className="twelve column bgPhk">
			      <div className="home">
			        <h4>PHOENIX KIDS</h4>
			      </div>
			    </div>
			  </div>
			 </div>
			 
			 <div className="mem-descp">
			   	<p>
				   	We are dedicated to expanding access to computer science and increasing participation by children particularly young girls and underrepresented minorities. Our vision is that every child should be exposed to 3D animation, Coding and Robotics. We organise training either at iSpace Foundation or at Schools and various community centres.
				</p>
			   	<a target="_blank" href="http://phoenixkids.co/">Read More On Our Website</a>
			 </div>
			 
			 <div className="container">
			  <div className="row">
			    <div className="twelve column bgimg4">
			      <div className="home">
			        <h4>UNLOCKED AT iSPACE</h4>
			      </div>
			    </div>
			  </div>
			 </div>
			 <div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="mem-descp">
			        <p>
			        	We know running a Startup can be a daunting challenge. Therefore it is very important to learn as much as possible to ensure that your startup has every chance for success. At iSpace, we have developed a series of workshops that are specifically suited to those who are new to business and want to learn more and also network with fellow entrepreneurs.
			        </p>
			        <a onClick={this.showBox.bind(this, "true")}>Sign Up</a>
			      </div>
			     </div>
			   </div>
			 </div>
			
			 <div className="container">
			  <div className="row">
			    <div className="twelve column bgimg5">
			      <div className="home">
			        <h4>iSPACE ACCELERATOR PROGRAM</h4>
			      </div>
			    </div>
			  </div>
			 </div>
			 <div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="mem-descp">
			        <p>
			        	Startup time here is limited to a 3 month period. The basic intention is to help jump-start your business ideas and then proceed into our Incubation Program.  The cash investment into a  startup from iSpace GHS2,000.00, but your time in our accelerator program should largely improve your chances of raising venture capital from a third party. You can also acquire customers after you graduate from the program.   
			        	Mentorship could be coming from a network of entrepreneurs that are affiliated with iSpace (many of which are proven Founders looking for the opportunity of helping the local startup community).
			        </p>
			        <a onClick={this.showBox.bind(this, "true")}>Apply</a>
			      </div>
			     </div>
			   </div>
			 </div>
		</div>
    );
  }

}



class Box extends React.Component {

	programRequest(e) {
	  	e.preventDefault();
	  	var name = e.target.name.value;
	    var email = e.target.email.value;
	   	var number = e.target.number.value;
	   	var program = e.target.program.value;

	    if (name !== "" && email !== "" && number !== "" && program !== "") {
		   	var data = {
		   		name: name,
		   		email: email,
		    	number: number,
		    	program: program,
		    }
		    programRequest(data)
		}

		e.target.name.value = "";
	    e.target.email.value = "";
	    e.target.number.value = "";
	    e.target.program.value = "";

	    $("form").hide();
	    $("#submitbox").append(`<div class='thanks'><h2>Thank You ${name}</h2><p>We will get back to you within 24 hours.</p></div>`);
	}

	render() {
	    return (
	    		<div id="submitbox">
			      	<h4>JOIN OUR PROGRAMS</h4>
			       	<form onSubmit={this.programRequest.bind(this)}>
			       		<input type="text" placeholder="Your Name" name="name"/>
			       		<input type="text" placeholder="Your Email" name="email" />
			       		<input type="text" placeholder="Your Number" name="number" />
			       		<select name="program">
			       			<option>Choose A Program</option>
			       			<option>UNLOCKED AT iSPACE</option>
			       			<option>iSPACE ACCELERATOR PROGRAM</option>
			       		</select>
			       		<button>SUBMIT</button>
			       	</form>
			       	<a onClick={this.props.box}>Cancel</a>
			    </div>
			)
	}

}





