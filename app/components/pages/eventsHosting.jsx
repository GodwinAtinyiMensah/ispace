import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import Tourbutton from '../layouts/tourButton';
import {spaceBooking} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Eventshosting extends React.Component {

  constructor(props) {
  	super(props)
    this.state = {
    	showBox: "false",
    }
  }

  showBox(option) {
  	this.setState({
  		showBox: option,
  	})
  }

  render() {
  	var box;
  	if (this.state.showBox == "false") {
  		box = ""
  	}else {
  		box = <Box box={this.showBox.bind(this, "false")} />
  	}
  	
    return (
		<div className="eventpage">
			{box}
			<div className="container">
			  <div className="row">
			    <div className="twelve column subheaders event-header1">
			      <div className="home">
			        <div className="logo">
			          <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
			        </div>
			        <div className="caption">
			          <h1>EVENT HOSTING</h1>
			          <div>
			           	<HyperLink name="UPCOMING EVENTS" link="#/event-upcoming" />
			            <a onClick={this.showBox.bind(this, "true")}>Book Our Space</a>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			 </div>
			
			<div className="container">
			   <div className="row">
			     <div className="twelve column">
			      <div className="subpage">
			       <div className="list">
						<div>
							<img src="ui/assets/images/events/Done/meetingroom.png" />
						    <h5>CONFERENCE ROOM</h5>
				      		<p>Our winning facilities are perfect for your office meetings medium & small.  The space can be either setup either contemporary to the traditional to suit your needs.</p>
						</div> 
						<div>
						    <img src="ui/assets/images/events/Done/garden.png" />
						    <h5> DELIGHTFUL GARDEN</h5>
				      		<p>Our garden provides a unique and relaxing setting for any event and outdoor working. Choose from a wide range of formal and casual setups which can accommodate from 20 to 40 guests.</p>
						</div>
						<div>
						    <img src="ui/assets/images/events/Done/nkrumahhall.png" />
						    <h5>NKRUMAH HALL</h5>
				      		<p>Are you looking for a space that is flexible in which to host your next workshop or event? Our main event hall is available for a wide variety of occasions;</p>
						</div>
						
					</div>
			      </div>
			     </div>
			   </div>
			</div>
			<div className="features">
			   <h5>WHAT DO WE PROVIDE ....</h5>
			   <div className="icons">
			     <div>
			       <i className="material-icons">location_on</i>
			       <h6>Air Conditioned Space</h6>
			     </div>
			     
			     <div>
			       <i className="material-icons">wifi</i>
			       <h6>Reliable Internet</h6>
			     </div>
			     <div>
			       <i className="material-icons">person_pin</i>
			       <h6>Dedicated On-site Support</h6>
			     </div>
			     <div>
			       <i className="material-icons">people_outline</i>
			       <h6>Video Conferencing</h6>
			     </div>
			     <div>
			       <i className="material-icons">settings</i>
			       <h6>Projectors and Audio Systems</h6>
			     </div>
			     <div>
			       <i className="material-icons">home</i>
			       <h6>Kitchen Access</h6>
			     </div>
			   </div>
			 </div>
		</div>
    );
  }

}


export class Box extends React.Component {
	spaceBooking(e) {
	  	e.preventDefault();
	  	var name = e.target.name.value;
	    var email = e.target.email.value;
	   	var number = e.target.number.value;
	    e.target.name.value = "";
	    e.target.email.value = "";
	    e.target.number.value = "";
	   	var data = {
	   		name: name,
	   		email: email,
	    	number: number,
	    }
	    spaceBooking(data)
	    $("form").hide();
	    $("#submitbox").append(`<div class='thanks'><h2>Thank You ${name}</h2><p>We will get back to you soon</p></div>`);
	}

	render() {
	    return (
	    		<div id="submitbox">
					<form onSubmit={this.spaceBooking.bind(this)}>
						<h4>BOOK OUR SPACE</h4>
				    	<input type="text" name="name" placeholder="Your Name" />
				    	<input type="email" name="email" placeholder="Your Email" />
					   	<input type="text" name="number" placeholder="Your Number" />
						<button>SUBMIT</button>
			    	</form>
			    	<a onClick={this.props.box}>Cancel</a>
			    </div>
			)
	}

}


