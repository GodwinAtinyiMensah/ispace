import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import Tourbutton from '../layouts/tourButton';
import {setUrl} from '../../actions/AppActions';
import {tourRequest} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Home extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showBox: "false",
    }
  }

  showBox(option) {
    this.setState({
      showBox: option,
    })
  }

  render() {
  	var box;
    if (this.state.showBox == "false") {
      box = ""
    }else {
      box = <Tourbutton box={this.showBox.bind(this, "false")} />
    }
    return (
		<div>
			{box}
			<div className="bgimg1">
		    <div className="home">
		    <div className="logo">
		      
		      <img className="mainimg" src="https://storage.googleapis.com/iimages/ispace.png" />
		    </div>
		    <div className="caption">
		      <h1>LEARN.CREATE.NETWORK</h1>
		      <div>
		        <HyperLink name="APPLY FOR CODE SCHOOL" link="#/programs" />
		        <button id="tour" onClick={this.showBox.bind(this, "true")}>REQUEST A TOUR</button>
		      </div>
		    </div>
		   </div>
		 </div>
		 <div className="center">
		   <div className="list logos">
		     	<div>
			     	<a href="http://1stSelections.com/" target="_blank"> <img src="https://storage.googleapis.com/iimages/1stSelections.png" /></a>
			    </div>
			    <div>
			       <a href="http://getflippify.com/" target="_blank"><img src="https://storage.googleapis.com/iimages/flippyfy.png" /></a>
			    </div>
		     	<div>
		       		<a href="http://mojaapp.net/" target="_blank"> <img src="https://storage.googleapis.com/iimages/mojaapp.png" /></a>
		     	</div>
		     	<div>
			    	<a href="http://www.heyshoeshine.com/" target="_blank"> <img src="https://storage.googleapis.com/iimages/shoeshine.png" /></a>
			 	</div>
			    <div>
			       <a href="http://www.adweninternship.com/" target="_blank"> <img src="https://storage.googleapis.com/iimages/adweninternship.png" /></a>
			    </div>
			    <div>
	              <a href="https://twitter.com/HarmattanRain" target="_blank"><img src="https://storage.googleapis.com/iimages/harmatanrain.jpg" /></a>
	            </div>
			    <div>
			     	<HyperLink name="MORE STARTUPS" link="#/community" classN="wine-button" />
			    </div>
		    </div>
		  </div>
		 <div className="container">
		  <div className="row">
		    <div className="twelve column bgimg2">
		      <div className="home">
		        <h4>OUR PROGRAMS</h4>
		      </div>
		    </div>
		  </div>
		 </div>
		 <div className="center">
		   <div className="list">
		     <div>
		       <h5>CODE SCHOOL</h5>
		       <p>
				Code School is a three part program comprising Beginner, Intermediate and Advanced classes that aims to train people on how to build Mobile and Web Apps.
			   </p>
		     </div>
		     <div>
		       <h5>UNLOCKING WOMEN & TECHNOLOGY</h5>
		       <p>Unlocking Women and Technology's aim is to harness the use of technology in mobilising women and empower them to innovate through technology.</p>
		     </div>
		     <div>
		       <h5>PHOENIX KIDS</h5>
		       <p>We are dedicated to expanding access to computer science and increasing participation by children particularly young girls and underrepresented minorities. </p>
		     </div>
		     <div>
		       <h5>UNLOCKED AT iSPACE</h5>
		       <p>We know running a Startup can be a daunting challenge. Therefore it is very important to learn as much as possible to ensure that your startup has every chance for success. .</p>
		     </div>
		     <div>
		       <h5>iSPACE ACCELERATOR PROGRAM</h5>
		       <p>Startup time here is limited to a 3 month period, the basic intention is to help jump start their business ideas and then proceed into our Incubation Program. </p>
		     </div>
		     <div className="block">
		     	<HyperLink name="READ MORE" link="#/programs" classN="wine-button" />
		     </div>
		    </div>
		  </div>
		 <div className="container">
		  <div className="row">
		    <div className="twelve column bgimg4">
		      <div className="home">
		        <h4>EVENTS</h4>
		      </div>
		    </div>
		  </div>
		 </div>
		 <div className="center">
		   <div className="dual-width">
		     <div>
		       <h5>HOST YOUR EVENTS WITH US.</h5>
		       <h6>HIRE OUR WONDERFUL SPACE.</h6>
		       <HyperLink name="GET STARTED" link="#/event-hosting" classN="button" />
		     </div>
		     <div>
		       <h5>ATTEND OUR EVENTS</h5>
		       <h6>WE HOST EVENTS FOR AND BY THE COMMUNITY.</h6>
		       <HyperLink name="UPCOMING EVENTS" link="#/event-upcoming" classN="button" />
		     </div>
		   </div>
		  </div>
		</div>
    );
  }

}






