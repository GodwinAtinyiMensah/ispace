import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {getTeamPlayer} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Team extends React.Component {

  constructor(props) {
  	super(props)
  	this.list = this.list.bind(this);
  }
  
  list(list) {
  	return list.map(function(item, i) {
  		return(
		    <div key={i}>
			    <img src={item.Picture}  onClick={() => {getTeamPlayer(item);location.hash = "#/profile"}} />
			    <h5  onClick={() => {getTeamPlayer(item);location.hash = "#/profile"}}>{item.Name}</h5>
			    <h6>{item.Position}</h6>
			    <a href={item.Social.facebook} target="blank"><i className="fa fa-facebook"></i></a>
			    <a href={item.Social.twitter} target="blank"><i className="fa fa-twitter"></i></a>
			    <a href={item.Social.linkedIn} target="blank"><i className="fa fa-linkedin"></i></a>
			</div>
  		)
  	})
  }


  routeAbout() {
  	window.scrollTo(0, 300)
  }

  routeTeam() {
  	window.scrollTo(0, 500)
  }

  render() {
    return (
		<div>
			<div className="bgimg1">
			    <div className="home">
				    <div className="logo">
				      <img className="mainimg" src="https://storage.googleapis.com/iimages/ispace.png" />
				    </div>
				    <div className="caption">
				      <h1>WHO WE ARE</h1>
				      <h2>FUELED BY INNOVATION</h2>
				      
				      <div>
				        <a onClick={this.routeTeam}>MEET THE TEAM</a>
				        <a onClick={this.routeAbout}>ABOUT US</a>
				      </div>
				    </div>
			   </div>
			 </div>
			 <div className="container" id="about">
			   <div className="row">
			     <div className="twelve column">
			      <div className="subpage">
			        <h4>ABOUT US</h4>
			        <p>
			        	Founded in 2013, we believe our role in the startup ecosystem  is to provide a conducive environment for growth, we achieve this by providing (i) Office Space (ii) Funding (iii) Training And Mentoring . 

		         		We are a space created for and by the local tech community. We welcome talent, ideas and also champion the use of mobile and web technology for Social Impact.
			        </p>
			      </div>
			     </div>
			   </div>
			 </div>
			 <div className="container" id="team">
			   <div className="row">
			     <div className="twelve column">
			      <div className="team">
			        <h4>MEET THE TEAM</h4>
			        <div className="list">
			          {this.list(this.props.data)}
			        </div>
			      </div>
			     </div>
			   </div>
			 </div>
		</div>
    );
  }

}






