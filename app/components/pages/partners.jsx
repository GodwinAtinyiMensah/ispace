import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import Tourbutton from '../layouts/tourButton';
import { partnerRequest} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Partners extends React.Component {

  constructor(props) {
  	super(props)
    this.state = {
    	showBox: "false",
    }
  }

  showBox(option) {
  	this.setState({
  		showBox: option,
  	})
  }

  render() {
  	var box;
  	if (this.state.showBox == "false") {
  		box = ""
  	}else {
  		box = <Box box={this.showBox.bind(this, "false")} />
  	}
    return (
		<div>
			{box}
			<div className="container">
			  <div className="row">
			    <div className="twelve column subheaders comm-header1">
			      <div className="home">
			        <div className="logo">
			          <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
			        </div>
			        <div className="caption">
			          <h1>OUR PARTNERS</h1>
			          <a onClick={this.showBox.bind(this, "true")}>PARTNER WITH US</a>
			        </div>
			      </div>
			    </div>
			  </div>
			 </div>
			
			 <div className="center">
			   <div className="list logos">
			     <div>
			     <a href="http://threesixtygh.com/" target="blank"><img src="https://storage.googleapis.com/iimages/partners/360gh.png" /></a>
			     </div>
			     <div>
				<a href="https://www.googleforentrepreneurs.com/" target="blank"><img src="https://storage.googleapis.com/iimages/partners/googleforentrepreneurs.png" /></a>
			     </div>
			     <div>
			      <a href="https://www.hivos.org/" target="blank"><img src="https://storage.googleapis.com/iimages/partners/hivosint.png" /></a>
			     </div>
			     <div>
			       <a href="http://www.makingallvoicescount.org/" target="blank"><img src="https://storage.googleapis.com/iimages/partners/mavclogo.png" /></a>
			     </div>
			     <div>
			       <a href="http://www.doen.nl/about-doen/general.htm" target="blank"><img src="https://storage.googleapis.com/iimages/partners/sticthtingdoen.png" /></a>
			     </div>
			     <div>
			       <a href="https://indigotrust.org.uk/" target="blank"><img src="https://storage.googleapis.com/iimages/partners/theindigotrust.png" /></a>
			     </div>
			     <div>
			       <a href="http://shmfoundation.org/" target="blank"><img src="https://storage.googleapis.com/iimages/partners/theshmfoundation.png" /></a>
			     </div>
			    </div>
			  </div>
			  
		</div>
    );
  }

}


export class Box extends React.Component {
	partnerRequest(e) {
	  	e.preventDefault();
	  	var name = e.target.name.value;
	    var email = e.target.email.value;
	   	var number = e.target.number.value;

	    if (name !== "" && email !== "" && number !== "") {
		   	var data = {
		   		name: name,
		   		email: email,
		    	number: number,
		    }
		    partnerRequest(data)
		}

		e.target.name.value = "";
	    e.target.email.value = "";
	    e.target.number.value = "";

	    $("form").hide();
	    $("#submitbox").append(`<div class='thanks'><h2>Thank You ${name}</h2><p>We will get back to you within 24 hours.</p></div>`);
	}

	render() {
	    return (
	    		<div id="submitbox">
					<form onSubmit={this.partnerRequest.bind(this)}>
						<h4>BECOME OUR PARTNER</h4>
				    	<input type="text" name="name" placeholder="Your Name" />
				    	<input type="email" name="email" placeholder="Your Email" />
					   	<input type="text" name="number" placeholder="Your Number" />
						<button>SUBMIT</button>
			    	</form>
			    	<a onClick={this.props.box}>Cancel</a>
			    </div>
			)
	}

}



