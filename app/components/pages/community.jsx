import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Community extends React.Component {

  render() {
    return (
		<div className="community">
			<div className="container">
			    <div className="row">
				    <div className="twelve column subheaders comm-header1">
				      <div className="home">
				        <div className="logo">
				         <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
				        </div>
				        <div className="caption">
				          <h1>OUR COMMUNITY</h1>
				          <div>
				            <HyperLink name="PARTNER WITH US" link="#/partners" />
				          </div>
				        </div>
				      </div>
				    </div>
			    </div>
			</div>
			<div className="logos">
			     <div>
			      <a href="http://getflippify.com/" target="_blank"><img src="https://storage.googleapis.com/iimages/flippyfy.png" /></a>
			     </div>
			     <div>
			       <a href="http://mojaapp.net/" target="_blank"> <img src="https://storage.googleapis.com/iimages/mojaapp.png" /></a>
			     </div>
			     <div>
			       <a href="http://www.heyshoeshine.com/" target="_blank"> <img src="https://storage.googleapis.com/iimages/shoeshine.png" /></a>
			     </div>
			     <div>
			       <a href="http://www.adweninternship.com/" target="_blank"> <img src="https://storage.googleapis.com/iimages/adweninternship.png" /></a>
			     </div>
			     <div>
			       <a href="http://1stSelections.com/" target="_blank"> <img src="https://storage.googleapis.com/iimages/1stSelections.png" /></a> 
			     </div>
			     <div>
			      <a href="http://tukwan.co/" target="_blank"> <img src="https://storage.googleapis.com/iimages/tukwan.jpg" /></a> 
			     </div>
			     <div>
			       <a href="http://www.tacoraonline.com/" target="_blank"><img src="https://storage.googleapis.com/iimages/tacoralogo.png" /></a> 
			    </div>
			    <div>
	              <a href="https://twitter.com/ajo_social" target="_blank"><img src="https://storage.googleapis.com/iimages/Ajo.jpg" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/DapperAfrique" target="_blank"><img src="https://storage.googleapis.com/iimages/dapperafrique.png" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/HarmattanRain" target="_blank"><img src="https://storage.googleapis.com/iimages/harmatanrain.jpg" /></a>
	            </div>
	            <div>
	              <a href="https://web.facebook.com/EasyLifeGH/" target="_blank"><img src="https://storage.googleapis.com/iimages/easylife.jpg" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/miraah2017" target="_blank"><img src="https://storage.googleapis.com/iimages/miras_logo.png" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/CodedModesty" target="_blank"><img src="https://storage.googleapis.com/iimages/modest.JPG" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/mydocgh" target="_blank"><img src="https://storage.googleapis.com/iimages/mydoc.jpg" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/Shop4meGh" target="_blank"><img src="https://storage.googleapis.com/iimages/shop4me.png" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/Tobreme" target="_blank"><img src="https://storage.googleapis.com/iimages/Tobreme.png" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/djaragroup" target="_blank"><img src="https://storage.googleapis.com/iimages/djara.png" /></a>
	            </div>
	            <div>
	              <a href="http://compogh.com/" target="_blank"><img src="https://storage.googleapis.com/iimages/1acrefarm.png" /></a>
	            </div>
	            <div>
	              <a href="https://twitter.com/mysaragh" target="_blank"><img src="https://storage.googleapis.com/iimages/sara.jpg" /></a>
	            </div>
	            <div>
	              <a href="http://truelooks.co/" target="_blank"><img src="https://storage.googleapis.com/iimages/TruLooks.png" /></a>
	            </div>
			</div>
		</div>
    );
  }

}
