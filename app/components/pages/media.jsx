import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import Videos from '../layouts/videos';
import Images from '../layouts/images';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Media extends React.Component {

  constructor(props) {
  	super(props)
  	this.state = {
  		page: "images",
  	}
  }

  togglePage(page) {
  	this.setState({
  		page: page,
  	})
  }
  
  render() {
  	var body;

  	// if(this.state.page == "videos") {
  	// 	body = <Videos list={this.props.videos} />
  	// }else {
  	// 	body = 
  	// }
    return (
		<div className="media">
			<div className="container">
			  <div className="row">
			    <div className="twelve column subheaders prog-header1">
			      <div className="home">
			        <div className="logo">
			          <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
			        </div>
			        <div className="caption">
			          <h1>GALLERY</h1>
			          <h4>Life at iSpace.</h4>
			        </div>
			      </div>
			    </div>
			  </div>
			 </div>
			 <div className="container">
			    <div className="row">
			     <div className="twelve column">
			      <div className="subpage">
			        <button id="tour" onClick={this.togglePage.bind(this, 'images')}>IMAGES</button>
			        <button id="tour" onClick={this.togglePage.bind(this, 'videos')}>VIDEOS</button>
			      </div>
			     </div>
			    </div>
			 </div>
			 <div className="container media">
			   <div className="row">
			     <div className="twelve column">
			     	<Images list={this.props.data} />
			     </div>
			   </div>
			 </div>
		</div>
    );
  }

}






