import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {getBlog} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Blog extends React.Component {

  constructor(props) {
  	super(props)
  	this.posts = this.posts.bind(this);
  }
  
  posts(list) {
  	return list.map(function(item, i) {
  		return(
  			<div key={i}>
			    <img src={"https://storage.googleapis.com/iblog/" + item.Pic} />
			    <p>{item.Title}</p>
			    <button onClick={() => {getBlog(item);location.hash = "#/blogpost"}}>READ MORE</button>
		    </div> 
  		)
  	})
  }

  render() {
    return (
		<div className="blog">
			<div className="container">
			  <div className="row">
			    <div className="twelve column subheaders prog-header1">
			      <div className="home">
			        <div className="logo">
			          <HyperLink img="https://storage.googleapis.com/iimages/ispace.png" link="#" />
			        </div>
			        <div className="caption">
			          <h1>BLOG</h1>
			          <h4>Our experiences so far.</h4>
			        </div>
			      </div>
			    </div>
			  </div>
			 </div>
			 
			 <div className="container blog">
			   <div className="row">
			     <div className="twelve column">
			      <div className="subpage">
			        <div className="list">
			          {this.posts(this.props.list)}
			          <div className="item">
			          	<div className="overlay"></div>
						<h4>daskdad</h4>
					    <img src="assets/images/bg1.jpg" />
				      </div> 
				      <div className="item">
				      	<div className="overlay"></div>
						<h4>daskdad</h4>
					    <img src="assets/images/bg1.jpg" />
				      </div> 
				      <div className="item">
				      	<div className="overlay"></div>
						<h4>daskdad</h4>
					    <img src="assets/images/bg1.jpg" />
				      </div> 
				      <div className="item">
			          	<div className="overlay"></div>
						<h4>daskdad</h4>
					    <img src="assets/images/bg1.jpg" />
				      </div> 
				      <div className="item">
				      	<div className="overlay"></div>
						<h4>daskdad</h4>
					    <img src="assets/images/bg1.jpg" />
				      </div> 
				      <div className="item">
				      	<div className="overlay"></div>
						<h4>daskdad</h4>
					    <img src="assets/images/bg1.jpg" />
				      </div> 
			        </div>
			      </div>
			     </div>
			   </div>
			 </div>
			 
		</div>
    );
  }

}






