import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {setUrl, signIn} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Dashboard extends React.Component {

  getInputs(e) {
    e.preventDefault();
    var email = e.target.email.value;
    var password = e.target.password.value;

    var user = {
      email: email,
      password: password
    }

    signIn(user);

    e.target.email.value = "";
    e.target.password.value = "";
  }

  render() {
    return (
	    <div className="welcome">
        <div className="overlay">
          <form onSubmit={this.getInputs} >
            <h3>iSpace</h3>
            <input type="email" name="email" placeholder="Your Email" />
            <input type="password" name="password" placeholder="Password" />
            <button>SUBMIT</button>
          </form>
        </div>
      </div>
    );
  }

}



