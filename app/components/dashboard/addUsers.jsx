import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {signUp} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Addusers extends React.Component {

  getInputs(e) {
    e.preventDefault();
    var fname = e.target.fname.value;
    var lname = e.target.lname.value;
    var email = e.target.email.value;
    var password = e.target.password.value;
    
    var user = {
      fname: fname,
      lname: lname,
      email: email,
      password: password
    }

    signUp(user);

    e.target.fname.value = "";
    e.target.lname.value = "";
    e.target.email.value = "";
    e.target.password.value = "";
  }


  render() {
    return (
	    <div className="contain">
        <div className="row">
          <div className="twelve columns addUsers">
            <form onSubmit={this.getInputs}>
              <h3>Add Users</h3>
              <input type="text" name="fname" placeholder="First Name" />
              <input type="text" name="lname" placeholder="Last Name" />
              <input type="email" name="email" placeholder="Email" />
              <input type="password" name="password" placeholder="Password" />
              <button>SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

}



