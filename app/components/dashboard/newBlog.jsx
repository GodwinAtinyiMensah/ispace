import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {addBlogPost} from '../../actions/AppActions'; 
import AppStore from '../../store/AppStore';

export default class Newblog extends React.Component {

  getInputs(e) {
    e.preventDefault();
    var ename = e.target.ename.value;
    var title = e.target.title.value;
    var pic = e.target.pic.files[0];
    var body = e.target.body.value;
    var formData = new FormData();
    formData.append('ename', ename);
    formData.append('title', title);
    formData.append('pic', pic);
    formData.append('body', ename);    
    addBlogPost(formData);

    e.target.ename.value = "";
    e.target.title.value = "";
    e.target.body.value = "";
  }

  render() {
    return (
	    <div className="contain">
        <div className="row">
          <div className="twelve columns blog">
            <form onSubmit={this.getInputs}>
              <h3>Add Blog Post</h3>
              <input type="text" name="ename" placeholder="Editors Name ...." />
              <input type="text" name="title" placeholder="Title ....." />
              <input type="file" name="pic" placeholder="Title ....." />
              <textarea name="body" placeholder="Body ........">
              </textarea>
              <button>SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

}



