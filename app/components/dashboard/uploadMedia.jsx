import React from 'react'; 
import $ from 'jquery';
import Images from '../layouts/imageuploads';
import Videos from '../layouts/videouploads';
import Gallery from '../layouts/gallery';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Uploadmedia extends React.Component {

  constructor(props) {
    super(props)
    this.state = AppStore.getStore();
    this._onChange = this._onChange.bind(this);
    this.state = {
      page: "gallery",
    }
  }


  _onChange() {
    this.setState(AppStore.getStore());
  }

  componentDidMount() {
    AppStore.addListener(this._onChange);
  }

  componentWillUnmount() {
    AppStore.removeListener(this._onChange)
  }

  route(data) {
    this.setState({
      page: data,
    })
  }

  render() {
    var body;
    if (this.state.page == "videos") {
      body = <Videos />
    }else if (this.state.page == "images") {
      body = <Images list={this.state.galleries}/>
    }else if (this.state.page == "gallery") {
      body = <Gallery />
    }
    return (
      <div>
        <div className="contain">
          <div className="row">
            <div className="twelve columns dmediaHead">
              <button onClick={this.route.bind(this, 'gallery')}>Create Gallery</button>
              <button onClick={this.route.bind(this, 'images')}>Upload Images</button>
              <button onClick={this.route.bind(this, 'videos')}>Upload Videos</button>
            </div>
          </div>
        </div>
        {body}
      </div>
	    
    );
  }

}



