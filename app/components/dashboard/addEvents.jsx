import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {addEvent} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Addevents extends React.Component {

  getInputs(e) {
    e.preventDefault();
    var name = e.target.name.value;
    var summary = e.target.summary.value;
    var link = e.target.link.value;
    var day = e.target.day.value;
    var month = e.target.month.value;
    var image = e.target.image.files[0];

    var formData = new FormData();

    formData.append('name', name);
    formData.append('summary', summary);
    formData.append('link', link);
    formData.append('day', day); 
    formData.append('month', month);  
    formData.append('image', image);  

    addEvent(formData);

    e.target.name.value = "";
    e.target.summary.value = "";
    e.target.link.value = "";
    e.target.day.value = "";
    e.target.month.value = "";
  }

  render() {
    return (
	    <div className="contain">
        <div className="row">
          <div className="twelve columns">
            <div className="event">
              <div>
                <form onSubmit={this.getInputs}>
                  <h3>New Event</h3>
                  <input type="text" name="name" placeholder="Name" />
                  <input type="text" name="summary" placeholder="Event summary" />
                  <input type="text" name="link" placeholder="Register Link" />
                  <input type="text" name="day" placeholder="Date" />
                  <input type="file" name="image" />
                  <select name="month">
                    <option>January</option>
                    <option>February</option>
                    <option>March</option>
                    <option>April</option>
                    <option>May</option>
                    <option>June</option>
                    <option>July</option>
                    <option>August</option>
                    <option>September</option>
                    <option>October</option>
                    <option>November</option>
                    <option>December</option>
                  </select>
                  <button>SUBMIT</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}


