import React from 'react'; 
import $ from 'jquery';
import HyperLink from './hyperLinks';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Menu extends React.Component {

  componentDidMount() {
  	$("#tourField").hide();
     document.querySelector(".fixedheader .img").style.display = "none";
     $(".fixedheader i").on("click", function() {
        document.querySelector(".menu").style.height = "100%";
     })
     $(".menu i").on("click", function() {
        document.querySelector(".menu").style.height = "0%";
     })
     $(".menu-content a").on("click", function() {
        document.querySelector(".menu").style.height = "0%";
     })
     window.addEventListener("scroll", function(e) {
        if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
          document.querySelector(".fixedheader").style.background = "white";
          document.querySelector(".fixedheader .img").style.display = "inline-block";
        } else if(document.body.scrollTop < 200 || document.documentElement.scrollTop < 200) {
          document.querySelector(".fixedheader").style.background = "transparent";
          document.querySelector(".fixedheader .img").style.display = "none";
        } 
     })
  }

  render() {
  	const font = {
  		fontSize: '50px',
  	}
    return (
	    <div>
			<div className="menu">
			    <i className="material-icons">cancel</i>
			    <div className="menu-content">
            <HyperLink name="WHO WE ARE" link="#/ourteam" />
            <HyperLink name="PROGRAMS" link="#/programs" />
            <HyperLink name="GALLERY" link="#/media" />
            <HyperLink name="COMMUNITY" link="#/community" />
            <HyperLink name="MEMBERSHIPS" link="#/memberships" />
            <HyperLink name="PARTNERS" link="#/partners" />
            <HyperLink name="CALENDAR" link="#/event-upcoming" />
			    </div>
			</div>
			<div className="fixedheader">
				<img className="img" src="https://storage.googleapis.com/iimages/ispace.png" />
				<i className="material-icons" style={{font}}>sort</i>
		    </div>
		</div>
    );
  }

}


// <HyperLink name="EVENTS HOSTING" link="#/event-hosting" /><HyperLink name="BLOG" link="#/blog" />
