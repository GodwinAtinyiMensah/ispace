import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {addGallery} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Gallery extends React.Component {

  getInputs(e) {
    e.preventDefault();
    var name = e.target.name.value;
    
    var gallery = {
      name: name
    }

    addGallery(gallery);

    e.target.name.value = "";
  }

  render() {

    return (
		<div className="contain">
	        <div className="row">
	          <div className="twelve columns dmedia">
	            <form onSubmit={this.getInputs}>
	              <h3>Create Gallery</h3>
	              <input type="text" name="name" placeholder="Gallery Name" />
	              <button>SUBMIT</button>
	            </form>
	          </div>
	        </div>
	    </div>
    );
  }

}






