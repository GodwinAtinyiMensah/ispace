import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {addVideo} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Videos extends React.Component {

  getInputs(e) {
    e.preventDefault();
    var name = e.target.name.value;
    var yl = e.target.yl.value;
    
    var video = {
      name: name,
      yl: yl
    }

    addVideo(video);

    e.target.name.value = "";
    e.target.yl.value = "";
  }
  
  render() {
    return (
		  <div className="contain">
        <div className="row">
          <div className="twelve columns dmedia">
            <form onSubmit={this.getInputs}>
              <h3>Upload Video</h3>
              <input type="text" name="name" placeholder="Name" />
              <input type="text" name="yl" placeholder="Youtube Link" />
              <button>SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

}






