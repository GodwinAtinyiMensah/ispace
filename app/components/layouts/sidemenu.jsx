import React from 'react'; 
import $ from 'jquery';
import HyperLink from './hyperLinks';
import {setUser} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Sidemenu extends React.Component {

  signout() {
    setUser({"LoggedIn": false})
  }

  render() {
    return (
	    <div className="sidemenu">
        <div>
          <h2>iSpace</h2>
          <li><HyperLink name="ADD USERS" link="#/dashboard/adduser" /></li>
          <li><HyperLink name="ADD EVENTS " link="#/dashboard/addevents" /></li>
          <li><HyperLink name="NEW BLOG POST" link="#/dashboard/newblog" /></li>
          <li><HyperLink name="UPLOAD MEDIA" link="#/dashboard/uploadmedia" /></li>
          <li>
            <a onClick={this.signout}>
              LOGOUT
            </a>
          </li>
        </div>    
      </div>
    );
  }

}



