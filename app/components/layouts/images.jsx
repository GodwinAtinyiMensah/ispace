import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {getGalleryView} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Images extends React.Component {

  constructor(props) {
  	super(props)
  	this.galleries = this.galleries.bind(this);
  }

  galleries(list) {
    if (list) {
      return list.map(function(item, i) {
        return (
          <div key={i} onClick={() => {getGalleryView(item);location.hash = "#/galleryview"}}>
            <img src={"https://storage.googleapis.com/igallary/" + item.Images[i].Name} />
          </div> 
        )
      })
    }
    
  }

  render() {

  	var body;
    return (
    	<div className="images">
			<div className="list">
				{this.galleries(this.props.list)}
			</div>
		</div>
    );
  }

}






