import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {setUrl} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Videos extends React.Component {

  constructor(props) {
    super(props)
    this.videos = this.videos.bind(this);
  }

  videos(list) {
    if (list) {
      return list.map(function(item, i) {
        return (
          <div key={i}>
            <iframe src={`${item.YL}`} frameBorder="0" allowFullScreen>
            </iframe>
            <p>{item.Name}</p>
          </div> 
        )
      })
    }
    
  }

  render() {
  	const style = {
  		background: '#e5e5e5',
  	}

  	var body;
    return (
        <div className="videos">
      		<div className="list">
            {this.videos(this.props.list)}
      		</div>
        </div>
    );
  }

}






