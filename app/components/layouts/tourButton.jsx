import React from 'react'; 
import $ from 'jquery';
import {tourRequest} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Tourbutton extends React.Component {

  tour(e) {
      e.preventDefault();
      var name = e.target.name.value;
      var email = e.target.email.value;
      var number = e.target.number.value;

      if (name !== "" && email !== "" && number !== "") {
        var data = {
          name: name,
          email: email,
          number: number,
        }
        tourRequest(data)
      }

      e.target.name.value = "";
      e.target.email.value = "";
      e.target.number.value = "";
      
      $("form").hide();
      $("#submitbox").append(`<div class='thanks'><h2>Thank You ${name}</h2><p>We will get back to you within 24 hours.</p></div>`);
  }

  render() {
      return (
          <div id="submitbox">
              
              <form onSubmit={this.tour.bind(this)}>
                <h4>Take a Tour</h4>
                <input type="text" placeholder="Your Name" name="name" required />
                <input type="text" placeholder="Your Email" name="email" required />
                <input type="text" placeholder="Your Number" name="number" required />
                <button>SUBMIT</button>
              </form>
              <a onClick={this.props.box}>Cancel</a>
          </div>
      )
  }

}





