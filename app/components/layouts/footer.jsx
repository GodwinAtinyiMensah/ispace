import React from 'react'; 
import $ from 'jquery';
import HyperLink from '../layouts/hyperLinks';
import {subscription} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class Footer extends React.Component {
  subscription(e) {
  	e.preventDefault();
  	var interests; 
  	var name = e.target.name.value;
    var email = e.target.email.value;
    var inputs = document.getElementsByName('interests');
    var interests = ""
    var i = 0;
    for (i = 0; i < inputs.length; ++i) {
	 	if (inputs[i].checked) {
	 		interests = interests + inputs[i].value + ", "
	    }
	}

    e.target.name.value = "";
    e.target.email.value = "";
   	var data = {
   		name: name,
   		email: email,
   		interests: interests
    }
    subscription(data)
    $("form").hide();
    $(".feedback").append(`<div class='thanks'><h2>Thank You ${name} For Subscribing.</h2>`);
    setTimeout(function() {
    	$("form").show();
    	$(".thanks").hide();
    }, 8000)
  }

  render() {
    return (
		<footer>
		  	<div className="layer1">
			    <div className="feedback">
			      <form onSubmit={this.subscription}>
			         <h5>Sign up for iSpace Updates</h5>	
			         <input type="text" name="name" placeholder="Name" required />
			         <input type="text" name="email" placeholder="Email" required />
			         <h6>Who are you ?</h6>
			         <section>
			         	<input type="checkbox" name="interests" value="Developer" />Developer
			         </section>
			         <section>
			         	<input type="checkbox" name="interests" value="Business Executives" checked />Business Executive
			         </section>
			         <section>
			         	<input type="checkbox" name="interests" value="Creatives" />Creative
			          </section>
			         <button>SUBMIT</button>
			       </form>
			  	</div>
			   	<div className="twitter-feeds">
			       <a className="twitter-timeline" data-width="420" data-height="300" href="https://twitter.com/iSpaceGh">National Park Tweets - Curated tweets by TwitterDev</a> 
			   	</div> 
		  	</div>
		  	<div className="layer2">
		       	<div className="icons">
		       		<a href="https://www.facebook.com/ispacegh" target="blank"><i className="fa fa-facebook"></i></a>
		       		<a href="https://twitter.com/iSpaceGh" target="blank"><i className="fa fa-twitter"></i></a>
		       		<a href="https://www.instagram.com/ispacegh/" target="blank"><i className="fa fa-instagram"></i></a>
		       		<a href="https://www.youtube.com/channel/UCBk2HpUvNkcUM4225RQUeJg#" target="blank"><i className="fa fa-youtube"></i></a>
		       	</div>
		       	<span>+233 50 655 6614</span>
		       	<span>info@ispacegh.com</span>
			   	<span>205/6 2nd Emmause Ln, Labone, Accra.</span>
			   </div>
		</footer>
    );
  }

}








