import React from 'react'; 
import $ from 'jquery';
/* 
PAGES
*/
import Menu from '../layouts/menu';
import Footer from '../layouts/footer';
import Home from '../pages/home';
import Memberships from '../pages/memberships';
import Programs from '../pages/programs';
import Community from '../pages/community';
import Eventshosting from '../pages/eventsHosting';
import Eventsupcoming from '../pages/eventsUpcoming';
import Partners from '../pages/partners';
import Blog from '../pages/blog';
import BlogPost from '../pages/blogpost';
import Team from '../pages/team'; 
import Media from '../pages/media';
import Galleryview from '../pages/galleryview';
import Profile from '../pages/profile';
import Projects from '../pages/projects';

/*
DASHBOARD
*/
import Dashboard from '../dashboard/dashboard';
import Addevents from '../dashboard/addEvents';
import Addusers from '../dashboard/addUsers';
import Newblog from '../dashboard/newBlog';
import Uploadmedia from '../dashboard/uploadMedia';
import Sidemenu from '../layouts/sidemenu';


import {setUrl, getEvents, getGallery, getCollection, getBlogPosts, getVideos} from '../../actions/AppActions';
import AppStore from '../../store/AppStore';

export default class App extends React.Component {

  constructor(props) {
  	super(props)
    this.state = AppStore.getStore();
    this._onChange = this._onChange.bind(this);
    this.pageAccess = this.pageAccess.bind(this);
  }

  _onChange() {
    this.setState(AppStore.getStore());
  }

  pageAccess() {
    if (this.state.user.LoggedIn == true) { 
      return
    }else {
      location.hash = ""
    }
  }


  componentDidMount() {
  	AppStore.addListener(this._onChange);
    // getEvents(); 
    getGallery();  
    getCollection();
    // getBlogPosts(); 
    // getVideos();
  }

  componentWillUnmount() {
    AppStore.removeListener(this._onChange)
  }

  render() {
  	var body;
    var menu;
    var footer;
  	switch(this.props.location) {
      case '#/dashboard':
        menu = ""
        body = <Dashboard />
        break
      case '#/dashboard/uploadmedia':
        this.pageAccess();
        menu = <Sidemenu />
        body = <Uploadmedia />
        break
      case '#/dashboard/spacebookings':
        this.pageAccess();
        menu = <Sidemenu />
        body = <Spacebookings />
        break
      case '#/dashboard/requestedtours':
        this.pageAccess();
        menu = <Sidemenu />
        body = <RequestedTours />
        break
      case '#/dashboard/newblog':
        this.pageAccess();
        menu = <Sidemenu />
        body = <Newblog />
        break
      case '#/dashboard/addevents':
        this.pageAccess();
        menu = <Sidemenu />
        body = <Addevents />
        break
      case '#/dashboard/adduser':
        this.pageAccess();
        menu = <Sidemenu />
        body = <Addusers />
        break
      case '#/memberships':
        menu = <Menu />
        body = <Memberships />
        footer = <Footer />
        break
      case '#/projects':
        menu = <Menu />
        body = <Projects />
        footer = <Footer />
        break
      case '#/programs':
        menu = <Menu />
        body = <Programs />
        footer = <Footer />
        break
      case '#/community':
        menu = <Menu />
        body = <Community />
        footer = <Footer />
        break
      case '#/event-hosting':
        location.hash = "#/event-upcoming"
        menu = <Menu />
        body = <Eventshosting />
        footer = <Footer />
        break
      case '#/event-upcoming':
        menu = <Menu />
        body = <Eventsupcoming list={this.state.events} />
        footer = <Footer />
        break
      case '#/partners':
        menu = <Menu />
        body = <Partners />
        footer = <Footer />
        break
      case '#/profile':
        menu = <Menu />
        body = <Profile data={this.state.teamplayer} />
        footer = <Footer />
        break
      case '#/ourteam':
        menu = <Menu />
        body = <Team data={this.state.team} />
        footer = <Footer />
        break
      case '#/blogpost':
        menu = <Menu />
        body = <BlogPost data={this.state.blogpage} />
        footer = <Footer />
        break
      case '#/blog':
        menu = <Menu />
        body = <Blog list={this.state.blogposts} />
        footer = <Footer />
        break
      case '#/media':
        menu = <Menu />
        body = <Media data={this.state.collection} videos={this.state.videos}/>
        footer = <Footer />
        break
      case '#/galleryview':
        menu = <Menu />
        body = <Galleryview list={this.state.galleryView} />
        footer = <Footer />
        break
      case '':
        menu = <Menu />
        body = <Home />
        footer = <Footer />
        break
      default:
        console.log()
        body = `<div>
            <h1>Not Found</h1>
          </div>`
        break
    }
    return (
	    <div>
        {menu}
			  {body}
			  {footer}
	  	</div>
    );
  }

}
