import AppDispatcher from '../dispatcher/AppDispatcher';
import { AppConstants } from '../constants/AppConstants';
import { signInApi, signUpApi, tourRequestApi, partnerRequestApi, programRequestApi, memberRequestApi, spaceBookingApi, subscriptionApi, getEventsApi, getGalleryApi, getCollectionApi, getBlogPostsApi, addEventApi, addBlogPostApi, addVideoApi, addGalleryApi, addImageApi, getVideosApi, applyApi} from "../utils/appAPI";

export function setUrl(url) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.SET_URL, 
    url: url
  });
}

export function tourRequest(data) {
  tourRequestApi(data)
}

export function partnerRequest(data) {
  partnerRequestApi(data)
}

export function programRequest(data) {
  programRequestApi(data)
}

export function memberRequest(data) {
  memberRequestApi(data)
}

export function spaceBooking(data) {
  spaceBookingApi(data)
}

export function subscription(data) {
  subscriptionApi(data)
}

// SET USER

export function setUser(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.SET_USER,
    data: data
  });
}

export function signUp(data) {
  signUpApi(data)
}

export function signIn(data) {
  signInApi(data)
}

export function addEvent(data) {
  addEventApi(data)
}

export function addBlogPost(data) {
  addBlogPostApi(data)
}

export function addVideo(data) {
  addVideoApi(data)
}

export function addGallery(data) {
  addGalleryApi(data)
}

export function setGallery(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.SET_GALLERY,
    data: data
  });
}

export function addImage(data) {
  addImageApi(data)
}



// LOAD DATA


export function getEvents() { 
  getEventsApi()
}

export function getGallery(data) {
  getGalleryApi()
}

export function setEvents(data) {
	AppDispatcher.handleViewAction({
		actionType: AppConstants.SET_EVENTS,
		data: data
	})
}

export function getCollection(data) {
  getCollectionApi(data)
}

export function setCollection(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.SET_COLLECTION,
    data: data
  })
}

export function getGalleryView(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.GET_GALLERY_VIEW,
    data: data
  })
}

export function getBlogPosts(data) {
  getBlogPostsApi()
}

export function getBlog(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.GET_BLOG,
    data: data
  })
}

export function setBlogPosts(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.SET_BLOG_POSTS,
    data: data
  })
}

export function getVideos(data) {
  getVideosApi()
}

export function setVideos(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.SET_VIDEOS,
    data: data
  })
}

export function getTeamPlayer(data) {
  AppDispatcher.handleViewAction({
    actionType: AppConstants.GET_TEAM_PLAYER,
    data: data
  })
}

export function apply(data) {
  applyApi(data)
}