export const Team = [
	{
		Name: "Josiah Kwesi Eyison",
		Position: "CEO",
		Picture: "https://storage.googleapis.com/iimages/team/jk.jpg",
		About: "Josiah has a well-rounded career in Communications with experience in Business Development to Financial Management. He has worked for and with Quintessentially, Learning Without Frontiers, Digital Arts, AmmoCity and Black Marvel Entertainments. He is also an avid Manchester United supporter",
		Social: {"linkedIn":"https://www.linkedin.com/in/josiaheyison/","facebook":"","twitter": "https://twitter.com/jkeyison"},
	},
	{
		Name: "Theresa Donkoh",
		Position: "Operations Manager",
		Picture: "https://storage.googleapis.com/iimages/team/t.jpg",
		About: "Enthused about nurturing startups especially female entrepreneurs. Passionate about organisational behavior, structure and culture.",
		Social: {"linkedIn":"https://www.linkedin.com/in/theresa-donkoh-48076982/","facebook":"https://www.facebook.com/tescyl","twitter": "https://twitter.com/tescyl"},
	},
	{
		Name: "Fiifi Baidoo",
		Position: "CTO",
		Picture: "https://storage.googleapis.com/iimages/team/fii.jpg",
		About: "Fiifi Baidoo is Co-founder of iSpace Foundation, a Ghana-based technology hub that works with global partners like IndigoTrust, Google for Entrepreneurs, Afriliabs, Oracle, Microsoft, Village Capital, MAVC  and Hivos to offer a co-working space, tools and facilities for entrepreneurs and startups to launch and manage their businesses. Fiifi Baidoo is a technology evangelist, his focus mainly is on promoting IoT and Big Data platform to users. He has 14 years of experience building software solutions for financial institutions, educational institutions and the telecommunication and mobile operators as a consultant in most cases. He is also a member of the Open Data Institute based in London and sits on a multi-stakeholder advisory committee for the National Data Roadmap implementation.",
		Social: {"linkedIn":"","facebook":"","twitter": "https://twitter.com/fiifibaidoo"},
	},
	{
		Name: "Favor Nma Ozichukwu",
		Position: "Programs Manager",
		Picture: "https://storage.googleapis.com/iimages/team/favor.jpeg",
		About: "Passionate about organising and developing strategies that are deliverable, enthused about the wholesome growth of start-ups, young entrepreneurs and innovative technology. Bsc Human Resource Management,Bilingual : Fluent in French and English.",
		Social: {"linkedIn":"https://www.linkedin.com/in/nma-ozichukwu-53418774/","facebook":"https://www.facebook.com/ozichukwu.nma","twitter": "https://twitter.com/OziNmaFav"},
	},
	{
		Name: "Enoch Appiah",
		Position: "Social Media Manager",
		Picture: "https://storage.googleapis.com/iimages/team/e.jpg",
		About: "Enoch moved from Kumasi to Accra. He studied Biology at University for Development studies - Navrongo Campus. His interest in Digital Marketing developed while in School. He proceeded to form the first ever secondhand store online, where most sales are driven through social media. \n He's passionate about Technology, SocialGood. In his free time, Enoch will be listening Coldplay album. ",
		Social: {"linkedIn":"https://www.linkedin.com/in/enochappiah/","facebook": "https://www.facebook.com/Djr.Appiah","twitter": "https://twitter.com/iamrobotboy"},
	},
	{
		Name: "Halima Haruna",
		Position: "Projects Manager",
		Picture: "https://storage.googleapis.com/iimages/team/hali.jpeg",
		About: "I come from a family of 6 where I'm the oldest sibling. I am a tech enthusiast and love to know how things work and be involved with new innovative ideas. I enjoy watching tv shows and hanging out with friends.",
		Social: {"linkedIn":"","facebook":"","twitter": ""},
	},
	{
		Name: "Philip Aye-Darko",
		Position: "Events & Community Mgr.",
		Picture: "https://storage.googleapis.com/iimages/team/phil.jpeg",
		About: "Studied Business Information Technology at University of Greenwich, Communication Studies at Central University. I love travelling and exploring places, Favourite place i have visited is James Town (British Accra). Enthused about new technology and how to implement them. Swimming, Reading and Lip singing are my relaxation therapy.",
		Social: {"linkedIn":"https://www.linkedin.com/in/philip-aye-darko-24020475/","facebook":"https://www.facebook.com/vuvulani.j.hammond","twitter": "https://twitter.com/kobe_tweets"},
	},
	{
		Name: "Suleiman Mohammed",
		Position: "Developer",
		Picture: "https://storage.googleapis.com/iimages/team/sul.jpeg",
		About: "Am sulley and I love coding . Coding allows me to bring my ideas to life . You will catch me watching naruto if am not coding.",
		Social: {"linkedIn":"https://www.linkedin.com/in/suleman-mohammed-058b6b100/","facebook":"https://www.facebook.com/mohammed.suleman.716","twitter": "https://twitter.com/wanted_suley"},
	}
]