import ReactDOM from 'react-dom'; 
import React from 'react';
import App from './components/containers/App';
import {setUrl} from './actions/AppActions';
import "./assets/styles/app.scss";
import "./assets/styles/normalize.scss";
import "./assets/styles/skeleton.scss";
import "../node_modules/toastr/build/toastr.min.scss";


function handleNewHash() {
  var location = window.location.hash;
  var application = <App location={location} />;
  ReactDOM.render(application, document.getElementById('app'));
} 

handleNewHash();
window.addEventListener('hashchange', handleNewHash, false);

