import AppDispatcher from "../dispatcher/AppDispatcher";
import {AppConstants} from "../constants/AppConstants";
import {Team} from "../constants/Team";
import {EventEmitter} from "events";

var sample = [
    {
        "Gallery": {
            "Name": "iSpace Accelerator Program",
            "Created": "0000-12-31T23:59:08+00:00",
            "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
        },
        "Images": [
            {
                "Name": "_DSC0530(2)-2.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0522(3).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0515.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0510(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0531(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0510.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0518(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0533(2).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0509.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            },
            {
                "Name": "_DSC0520(3).jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"
            }
        ]
    },
    {
        "Gallery": {
            "Name": "Phoenix Kids",
            "Created": "0000-12-31T23:59:08+00:00",
            "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
        },
        "Images": [
            {
                "Name": "DSC_9340.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-103.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-119.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-114.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "DSC_9350.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "DSC_9335.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "DSC_9353.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            },
            {
                "Name": "ispace-142.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "8febd14e-c2b9-40d8-a425-0f2069c3678c"
            }
        ]
    },
    {
        "Gallery": {
            "Name": "Unlocking Women And Technology",
            "Created": "0000-12-31T23:59:08+00:00",
            "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
        },
        "Images": [
            {
                "Name": "ddsseee.jpg",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0049.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_1310.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0048.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0035.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            },
            {
                "Name": "DSC_0046.JPG",
                "Created": "0000-12-31T23:59:08+00:00",
                "Id": "46fe4fb0-2958-46bb-ba26-a8c69780fa64"
            }
        ]
    }
]

var y = [{"Name":"iSpace Accelerator Program","Created":"0001-01-01T00:00:00Z","Id":"0cc3e90f-88a4-45a7-9ae4-d70501b0a3de"},{"Name":"Phoenix Kids","Created":"0001-01-01T00:00:00Z","Id":"8febd14e-c2b9-40d8-a425-0f2069c3678c"},{"Name":"","Created":"0001-01-01T00:00:00Z","Id":"749491f8-0602-4ada-9c70-780705b7bbda"},{"Name":"Unlocking Women And Technology","Created":"0001-01-01T00:00:00Z","Id":"46fe4fb0-2958-46bb-ba26-a8c69780fa64"}]


var CHANGE_EVENT = "change";

var _store = {
	url: "",
  events: [],
  user: {},
  galleries: [],
  collection: [],
  blogposts: [],
  blogpage: {},
  videos: [],
  galleryView: [],
  team: Team,
  teamplayer: {},
}

class StoreClass extends EventEmitter {
	addListener(cb) {
		this.on(CHANGE_EVENT, cb)
	}

	setUrl(url = location.pathname) {
		return _store.url = url;
	}

  setUser(user) {
    return _store.user = user;
  }

  setEvents(data) {
    return _store.events = JSON.parse(data);
  }

  setGallery(data) {
    return _store.galleries = data;
  }

  setCollection(data) {
    return _store.collection = data;
  }

  getGalleryView(data) {
    return _store.galleryView = data;
  }

  setBlogPosts(data) {
    return _store.blogposts = JSON.parse(data);
  }

  getBlog(data) {
    return _store.blogpage = data;
  }

  setVideos(data) {
    return _store.videos = JSON.parse(data);
  }

  getTeamPlayer(data) {
    return _store.teamplayer = data;
  }

	removeListener(cb){
		this.on(CHANGE_EVENT, cb)
	}

	getStore() {
		return _store; 
	}
}

const AppStore = new StoreClass(); 


AppDispatcher.register((payload) => {
  const action = payload.action;
  switch(action.actionType) {

  	case AppConstants.SET_URL:
  		AppStore.setUrl(action.url)
  		AppStore.emit(CHANGE_EVENT)
  		break;
    case AppConstants.SET_EVENTS:
      AppStore.setEvents(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.SET_USER:
      AppStore.setUser(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.SET_GALLERY:
      AppStore.setGallery(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.SET_COLLECTION:
      AppStore.setCollection(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.GET_GALLERY_VIEW:
      AppStore.getGalleryView(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.GET_BLOG:
      AppStore.getBlog(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.SET_BLOG_POSTS:
      AppStore.setBlogPosts(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.SET_VIDEOS:
      AppStore.setVideos(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
    case AppConstants.GET_TEAM_PLAYER:
      AppStore.getTeamPlayer(action.data)
      AppStore.emit(CHANGE_EVENT)
      break;
  }
  return true;
})

export default AppStore;