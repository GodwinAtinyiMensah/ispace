package main

import (
	"cloud.google.com/go/datastore"
	"encoding/json"
	//"fmt"
	"bytes"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	storage "google.golang.org/api/storage/v1"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"strings"
	"time"
)

var projID = "ispace-website"
var ctx = context.Background()
var client, _ = datastore.NewClient(ctx, projID)

const scope = storage.DevstorageFullControlScope

var templates, templates_err = template.ParseFiles("index.html")

func fatalf(service *storage.Service, errorMessage string, args ...interface{}) {
	log.Fatalf("Dying with error:\n"+errorMessage, args...)
}

type User struct {
	Fname    string    `datastore:"fname"`
	Lname    string    `datastore:"lname"`
	Email    string    `datastore:"email"`
	Password string    `datastore:"password"`
	Created  time.Time `datastore:"created"`
	Id       string    `datastore:"id"`
}

type isLoggedIn struct {
	User     User
	LoggedIn bool
}

type Blog struct {
	Ename   string    `datastore:"ename"`
	Title   string    `datastore:"title"`
	Pic     string    `datastore:"pic"`
	Body    string    `datastore:"body"`
	Created time.Time `datastore:"created"`
	Id      string    `datastore:"id"`
}

type Application struct {
	Fname   string `datastore:"fname"`
	Lname   string `datastore:"lname"`
	Email   string `datastore:"email"`
	Number  string `datastore:"number"`
	Course  string `datastore:"course"`
	Gender  string `datastore:"gender"`
	City    string `datastore:"city"`
	Region  string `datastore:"region"`
	Reason  string `datastore:"reason"`
	Created string `datastore:"created"`
	Id      string `datastore:"id"`
}

type Event struct {
	Name    string    `datastore:"name"`
	Summary string    `datastore:"summary"`
	Day     string    `datastore:"day"`
	Month   string    `datastore:"month"`
	Link    string    `datastore:"link"`
	Image   string    `datastore:"image"`
	Created time.Time `datastore:"created"`
	Id      string    `datastore:"id"`
}

type Video struct {
	Name    string    `datastore:"name"`
	YL      string    `datastore:"yl"`
	Created time.Time `datastore:"created"`
	Id      string    `datastore:"id"`
}

type Gallery struct {
	Name    string    `datastore:"name"`
	Created time.Time `datastore:"created"`
	Id      string    `datastore:"id"`
}

type Image struct {
	Name    string    `datastore:"name"`
	Created time.Time `datastore:"created"`
	Id      string    `datastore:"id"`
}

type Collection struct {
	Gallery Gallery
	Images  []Image
}

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/api/login", Login)
	http.HandleFunc("/api/signup", SignUp)
	http.HandleFunc("/api/addEvents", addEvents)
	http.HandleFunc("/api/getEvents", getEvents)
	http.HandleFunc("/api/newBlogPost", newBlogPost)
	http.HandleFunc("/api/getBlogPost", getBlogPost)
	http.HandleFunc("/api/addVideo", addVideo)
	http.HandleFunc("/api/getVideo", getVideo)
	http.HandleFunc("/api/addGallery", addGallery)
	http.HandleFunc("/api/getGallery", getGallery)
	http.HandleFunc("/api/getCollection", getCollection)
	http.HandleFunc("/api/addImage", addImage)
	http.HandleFunc("/api/tourRequest", tourRequest)
	http.HandleFunc("/api/spaceBooking", spaceBooking)
	http.HandleFunc("/api/memberRequest", memberRequest)
	http.HandleFunc("/api/partnerRequest", partnerRequest)
	http.HandleFunc("/api/programRequest", programRequest)
	http.HandleFunc("/api/subscription", subscription)
	http.HandleFunc("/api/apply", apply)
	http.Handle("/app/", http.FileServer(http.Dir(".")))
	log.Println("SERVING 8080")
	http.ListenAndServe(":8080", nil)
}

func index(w http.ResponseWriter, req *http.Request) {
	templates.Execute(w, "")
}

func SignUp(w http.ResponseWriter, rw *http.Request) {

	fname := rw.FormValue("fname")
	lname := rw.FormValue("lname")
	email := rw.FormValue("email")
	password := rw.FormValue("password")

	sID := uuid.NewV4()

	user := &User{
		Fname:    fname,
		Lname:    lname,
		Email:    email,
		Password: password,
		Created:  time.Now(),
		Id:       sID.String(),
	}

	key := datastore.IncompleteKey("Users", nil)
	log.Println(key)

	it, err := client.Put(ctx, key, user)
	if err != nil {
		log.Fatalf("Error fetching next task: %v", err)
	}

	log.Println(it.ID)

}

func Login(w http.ResponseWriter, req *http.Request) {

	var user User

	email := req.FormValue("email")
	pass := req.FormValue("password")

	if email != "" {
		query := datastore.NewQuery("Users").Filter("email =", email)
		it := client.Run(ctx, query)
		_, err := it.Next(&user)
		if err != nil {
			log.Fatalf("login: i amthe problem %v", err)
		}

		if pass == user.Password {
			userLogIn := &isLoggedIn{
				User:     user,
				LoggedIn: true,
			}
			currentUser, err := json.Marshal(userLogIn)
			if err != nil {
				log.Println(err)
			}
			w.Write([]byte(currentUser))
		}
	}

}

func LogOut(w http.ResponseWriter, req *http.Request) {
	c := &http.Cookie{
		Name:   "ispace",
		Value:  "",
		MaxAge: -1,
	}

	http.SetCookie(w, c)
	http.Redirect(w, req, "/", http.StatusFound)
}

func tourRequest(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	email := req.FormValue("email")
	number := req.FormValue("number")

	recieverEmail := "info@ispacegh.com"
	senderEmail := "suley@ispacegh.com"
	senderPass := "mosalis119988"
	msg := "From: " + senderEmail + "\n" +
		"To: " + recieverEmail + "\n" +
		"Subject: " + "ISPACE TOUR REQUEST \n\n" +
		"Hello Philip, \n\n" +
		"Someone has resquested for a tour around Ispace contact him on this line." + "See details \n\n" +
		"Name: " + name + "\n" +
		"Email: " + email + "\n" +
		"Number: " + number

	sent := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", senderEmail, senderPass, "smtp.gmail.com"),
		senderEmail, []string{recieverEmail}, []byte(msg))

	if sent != nil {
		log.Printf("smtp error: %s", sent)
	}

}

func spaceBooking(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	email := req.FormValue("email")
	number := req.FormValue("number")

	recieverEmail := "info@ispacegh.com"
	senderEmail := "suley@ispacegh.com"
	senderPass := "mosalis119988"
	msg := "From: " + senderEmail + "\n" +
		"To: " + recieverEmail + "\n" +
		"Subject: " + "SPACE BOOKINGS \n\n" +
		"Hello Philip, \n\n" +
		"Someone is interested in booking the space. See details below \n\n" +
		"Name: " + name + "\n" +
		"Email: " + email + "\n" +
		"Number: " + number

	sent := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", senderEmail, senderPass, "smtp.gmail.com"),
		senderEmail, []string{recieverEmail}, []byte(msg))

	if sent != nil {
		log.Printf("smtp error: %s", sent)
	}
}

func memberRequest(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	email := req.FormValue("email")
	number := req.FormValue("number")
	membership := req.FormValue("membership")

	recieverEmail := "info@ispacegh.com"
	senderEmail := "suley@ispacegh.com"
	senderPass := "mosalis119988"
	msg := "From: " + senderEmail + "\n" +
		"To: " + recieverEmail + "\n" +
		"Subject: " + "ISPACE MEMBERSHIP REQUEST \n\n" +
		"Hello Philip, \n\n" +
		"Someone is interested in ispace memberships. See details below \n\n" +
		"Name: " + name + "\n" +
		"Email: " + email + "\n" +
		"Number: " + number + "\n" +
		"Membership: " + membership

	sent := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", senderEmail, senderPass, "smtp.gmail.com"),
		senderEmail, []string{recieverEmail}, []byte(msg))

	if sent != nil {
		log.Printf("smtp error: %s", sent)
	}
}

func partnerRequest(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	email := req.FormValue("email")
	number := req.FormValue("number")

	recieverEmail := "kwesi@ispacegh.com"
	senderEmail := "suley@ispacegh.com"
	senderPass := "mosalis119988"
	msg := "From: " + senderEmail + "\n" +
		"To: " + recieverEmail + "\n" +
		"Subject: " + "ISPACE PATNERSHIPS \n\n" +
		"Hello Kwesi, \n\n" +
		"Someone is interested in ispace partnership. See details below \n\n" +
		"Name: " + name + "\n" +
		"Email: " + email + "\n" +
		"Number: " + number

	sent := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", senderEmail, senderPass, "smtp.gmail.com"),
		senderEmail, []string{recieverEmail}, []byte(msg))

	if sent != nil {
		log.Printf("smtp error: %s", sent)
	}
}

func programRequest(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	email := req.FormValue("email")
	number := req.FormValue("number")
	program := req.FormValue("program")

	recieverEmail := "info@ispacegh.com" + "," + "enoch@ispacegh.com" + "," + "philip@ispacegh.com"
	senderEmail := "suley@ispacegh.com"
	senderPass := "mosalis119988"
	msg := "From: " + senderEmail + "\n" +
		"To: " + recieverEmail + "\n" +
		"Subject: " + "ISPACE PROGRAMS REQUEST \n\n" +
		"Hello Favor, \n\n" +
		"Someone is interested in ispace programs. See details below \n\n" +
		"Name: " + name + "\n" +
		"Email: " + email + "\n" +
		"Number: " + number + "\n" +
		"Program: " + program

	sent := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", senderEmail, senderPass, "smtp.gmail.com"),
		senderEmail, []string{recieverEmail}, []byte(msg))

	if sent != nil {
		log.Printf("smtp error: %s", sent)
	}
}

func subscription(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	email := req.FormValue("email")
	interests := req.FormValue("interests")

	recieverEmail := "enoch@ispacegh.com"
	senderEmail := "suley@ispacegh.com"
	senderPass := "mosalis119988"
	msg := "From: " + senderEmail + "\n" +
		"To: " + recieverEmail + "\n" +
		"Subject: " + "ISPACE SUBSCRIPTION \n\n" +
		"Hello Enoch, \n\n" +
		"Someone wants to be a subscriber. See details below \n\n" +
		"Name: " + name + "\n" +
		"Email: " + email + "\n" +
		"Occupation: " + interests

	sent := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", senderEmail, senderPass, "smtp.gmail.com"),
		senderEmail, []string{recieverEmail}, []byte(msg))

	if sent != nil {
		log.Printf("smtp error: %s", sent)
	}
}

func newBlogPost(w http.ResponseWriter, req *http.Request) {
	ename := req.FormValue("ename")
	title := req.FormValue("title")
	body := req.FormValue("body")
	file, handler, err := req.FormFile("pic")
	data, err := ioutil.ReadAll(file)
	pic := handler.Filename
	pic = strings.Replace(pic, " ", "", -1)
	if pic != "" {
		datafile := bytes.NewReader(data[:])
		buff := []io.Reader{datafile}
		combined := io.MultiReader(buff...)
		// Authentication is provided by the gcloud tool when running locally, and
		// by the associated service account when running on Compute Engine.
		client, err := google.DefaultClient(context.Background(), scope)
		if err != nil {
			log.Fatalf("Unable to get default client: %v", err)
		}
		service, err := storage.New(client)
		if err != nil {
			log.Fatalf("Unable to create storage service: %v", err)
		}

		// Insert an object into a bucket.
		object := &storage.Object{Name: pic}

		if res, err := service.Objects.Insert("iblog", object).Media(combined).Do(); err == nil {
			log.Printf("Created object %v at location %v\n\n", res.Name, res.SelfLink)
		} else {
			fatalf(service, "Objects.Insert failed: %v", err)
		}
	}
	sID := uuid.NewV4()

	blog := &Blog{
		Ename:   ename,
		Title:   title,
		Pic:     pic,
		Body:    body,
		Created: time.Now(),
		Id:      sID.String(),
	}

	key := datastore.IncompleteKey("BlogPosts", nil)
	log.Println(key)

	it, err := client.Put(ctx, key, blog)
	if err != nil {
		log.Fatalf("Error fetching next task: %v", err)
	}
	log.Println(it.ID)
	w.Write([]byte("done"))

}

func getBlogPost(w http.ResponseWriter, req *http.Request) {
	var posts []Blog
	query := datastore.NewQuery("BlogPosts").Order("created")
	client.GetAll(ctx, query, &posts)
	list, err := json.Marshal(posts)
	if err != nil {
		log.Println(err)
	}
	w.Write([]byte(list))
}

func addEvents(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	summary := req.FormValue("summary")
	day := req.FormValue("day")
	link := req.FormValue("link")
	month := req.FormValue("month")
	file, handler, err := req.FormFile("image")
	if err != nil {
		log.Fatalf("Error error pushing image: %v", err)
	}
	data, err := ioutil.ReadAll(file)
	sID := uuid.NewV4()

	pic := handler.Filename
	if pic != "" {
		pic = strings.Replace(pic, " ", "", -1)
		datafile := bytes.NewReader(data[:])
		buff := []io.Reader{datafile}
		combined := io.MultiReader(buff...)
		// Authentication is provided by the gcloud tool when running locally, and
		// by the associated service account when running on Compute Engine.
		client, err := google.DefaultClient(context.Background(), scope)
		if err != nil {
			log.Fatalf("Unable to get default client: %v", err)
		}
		service, err := storage.New(client)
		if err != nil {
			log.Fatalf("Unable to create storage service: %v", err)
		}

		// Insert an object into a bucket.
		object := &storage.Object{Name: pic}

		if res, err := service.Objects.Insert("ievents", object).Media(combined).Do(); err == nil {
			log.Printf("Created object %v at location %v\n\n", res.Name, res.SelfLink)
		} else {
			fatalf(service, "Objects.Insert failed: %v", err)
		}
	}

	event := &Event{
		Name:    name,
		Summary: summary,
		Link:    link,
		Day:     day,
		Month:   month,
		Image:   pic,
		Id:      sID.String(),
	}

	key := datastore.IncompleteKey("Events", nil)

	_, err2 := client.Put(ctx, key, event)
	if err2 != nil {
		log.Fatalf("Error fetching next task: %v", err)
	}
	w.Write([]byte("done"))
}

func getEvents(w http.ResponseWriter, req *http.Request) {
	var events []Event
	query := datastore.NewQuery("Events").Order("created")
	client.GetAll(ctx, query, &events)
	list, err := json.Marshal(events)
	if err != nil {
		log.Println(err)
	}
	w.Write([]byte(list))
}

func addVideo(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	yl := req.FormValue("yl")
	sID := uuid.NewV4()

	video := &Video{
		Name: name,
		YL:   yl,
		Id:   sID.String(),
	}
	key := datastore.IncompleteKey("Videos", nil)

	_, err := client.Put(ctx, key, video)
	if err != nil {
		log.Fatalf("Error fetching next task: %v", err)
	}
	w.Write([]byte("done"))
}

func getVideo(w http.ResponseWriter, req *http.Request) {
	var videos []Video
	query := datastore.NewQuery("Videos").Order("created")
	client.GetAll(ctx, query, &videos)
	list, err := json.Marshal(videos)
	if err != nil {
		log.Println(err)
	}
	w.Write([]byte(list))
}

func addGallery(w http.ResponseWriter, req *http.Request) {
	name := req.FormValue("name")
	sID := uuid.NewV4()

	gallery := &Gallery{
		Name: name,
		Id:   sID.String(),
	}
	key := datastore.IncompleteKey("Gallerys", nil)

	_, err := client.Put(ctx, key, gallery)
	if err != nil {
		log.Fatalf("Error fetching next task: %v", err)
	}
	w.Write([]byte("done"))
}

func addImage(w http.ResponseWriter, req *http.Request) {
	gallery_id := req.FormValue("id")
	file, handler, err := req.FormFile("image")
	data, err := ioutil.ReadAll(file)
	pic := handler.Filename
	if pic != "" {
		pic = strings.Replace(pic, " ", "", -1)
		datafile := bytes.NewReader(data[:])
		buff := []io.Reader{datafile}
		combined := io.MultiReader(buff...)
		// Authentication is provided by the gcloud tool when running locally, and
		// by the associated service account when running on Compute Engine.
		client, err := google.DefaultClient(context.Background(), scope)
		if err != nil {
			log.Fatalf("Unable to get default client: %v", err)
		}
		service, err := storage.New(client)
		if err != nil {
			log.Fatalf("Unable to create storage service: %v", err)
		}

		// Insert an object into a bucket.
		object := &storage.Object{Name: pic}

		if res, err := service.Objects.Insert("igallary", object).Media(combined).Do(); err == nil {
			log.Printf("Created object %v at location %v\n\n", res.Name, res.SelfLink)
		} else {
			fatalf(service, "Objects.Insert failed: %v", err)
		}
	}

	image := &Image{
		Name: pic,
		Id:   gallery_id,
	}
	key := datastore.IncompleteKey("Images", nil)

	_, err = client.Put(ctx, key, image)
	if err != nil {
		log.Fatalf("Error fetching next task: %v", err)
	}
	w.Write([]byte("done"))
}

func getGallery(w http.ResponseWriter, req *http.Request) {
	var gallery []Gallery
	query := datastore.NewQuery("Gallerys").Order("created")
	client.GetAll(ctx, query, &gallery)
	list, err := json.Marshal(gallery)
	if err != nil {
		log.Println(err)
	}
	w.Write([]byte(list))
}

func getCollection(w http.ResponseWriter, req *http.Request) {

	var galleryList []Gallery
	var collection []Collection

	query := datastore.NewQuery("Gallerys").Order("created")
	client.GetAll(ctx, query, &galleryList)

	for i := 0; i < len(galleryList); i++ {
		var id = galleryList[i].Id
		var images []Image
		query := datastore.NewQuery("Images").Filter("id =", id)
		client.GetAll(ctx, query, &images)

		collection = append(collection, Collection{Gallery: galleryList[i], Images: images})
	}

	list, err := json.MarshalIndent(collection, "", "    ")
	if err != nil {
		log.Println(err)
	}
	w.Write([]byte(list))
}

func apply(w http.ResponseWriter, req *http.Request) {

	fname := req.FormValue("fname")
	lname := req.FormValue("lname")
	email := req.FormValue("email")
	number := req.FormValue("number")
	course := req.FormValue("course")
	gender := req.FormValue("gender")
	city := req.FormValue("city")
	region := req.FormValue("region")
	reason := req.FormValue("reason")
	sID := uuid.NewV4()

	app := &Application{
		Fname:   fname,
		Lname:   lname,
		Email:   email,
		Number:  number,
		Course:  course,
		Gender:  gender,
		City:    city,
		Region:  region,
		Reason:  reason,
		Created: time.Now().Local().Format("2006-01-02"),
		Id:      sID.String(),
	}

	key := datastore.IncompleteKey("CodeSchool", nil)
	log.Println(key)

	_, err := client.Put(ctx, key, app)
	if err != nil {
		log.Fatalf("Error fetching next task: %v", err)
	}

}
