var ExtractTextPlugin = require('extract-text-webpack-plugin');

var webpack = require('webpack');

var CopyWebpackPlugin = require('copy-webpack-plugin');

var HtmlWebpackPlugin = require('html-webpack-plugin');

var Extractcss = new ExtractTextPlugin('app.css');

// var CopyAssets = new CopyWebpackPlugin([
//     { from: 'app/assets/images', to: "./assets/images" },
//     { from: 'app/assets/video', to: "./assets/video" }
// ]);
var CopyAssets = new CopyWebpackPlugin([
    { from: 'app/assets', to: "assets/" }
]);

var path = require("path");

var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html'
});

module.exports = {
  entry: [
    './app/initialize.js'
  ],
  resolve: {
    root: path.resolve(__dirname),
    alias: {
      "~": "src"
    },
    extensions: ["", ".js", ".jsx", "scss"]
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        include:  __dirname + '/app',
        loader: ['babel-loader'],
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.scss$/,
        loaders: ExtractTextPlugin.extract({
          fallbackLoader: "style-loader",
          loader: "css-loader!sass-loader",
        })
      },
      {
        test: /\.(jpeg|jpg|png)$/, 
        loader: 'file-loader?name=./assets/[name].[ext]'
      }
    ]
  },
  output: {
    filename: 'index_bundle.js',
    path: __dirname + '/public/app/'
  },
  plugins: [HTMLWebpackPluginConfig, Extractcss, CopyAssets]
}